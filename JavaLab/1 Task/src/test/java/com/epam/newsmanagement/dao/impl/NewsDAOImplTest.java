package com.epam.newsmanagement.dao.impl;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.sql.Date;

import org.dbunit.DatabaseUnitException;
import org.dbunit.assertion.DbUnitAssert;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.epam.newsmanagement.dao.impl.NewsDAOImpl;
import com.epam.newsmanagement.entity.dto.AuthorDTO;
import com.epam.newsmanagement.entity.dto.NewsDTO;
import com.epam.newsmanagement.entity.dto.TagDTO;
import com.epam.newsmanagement.exception.PersistenceException;
import com.epam.newsmanagement.util.SearchCriteria;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DbUnitConfiguration;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/test-config.xml"})
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
@DbUnitConfiguration(databaseConnection={"testDatabaseConnection"})
@DatabaseSetup(value="/testdata/initialData.xml")
public class NewsDAOImplTest {

	@Autowired
	private NewsDAOImpl newsDAO;
	
	@Autowired
	private IDatabaseConnection databaseConnection;
	
	@Test
	public void testCreate() throws PersistenceException, IOException, SQLException, DatabaseUnitException{
		NewsDTO news = new NewsDTO();
		news.setTitle("Title 1");
		news.setShortText("Short text 1");
		news.setFullText("Full text 1");
		news.setCreationDate(Timestamp.valueOf("2015-11-11 11:38:47.79"));
		news.setModificationDate(Date.valueOf("2015-11-11"));
		newsDAO.create(news);
		
		FlatXmlDataSetBuilder xmlBuilder = new FlatXmlDataSetBuilder();
		InputStream inputStream = ClassLoader.getSystemResourceAsStream("testdata/afterInsertData.xml");
		IDataSet expectedDataset = xmlBuilder.build(new InputStreamReader(inputStream, "UTF-8"));	
		IDataSet actualDataset = databaseConnection.createDataSet();

		(new DbUnitAssert()).assertEqualsIgnoreCols(expectedDataset, actualDataset
				,"NEWS", new String[]{"NEWS_ID"});
	}
	
	@Test
	public void testFetch() throws PersistenceException{
		NewsDTO expectedNews, actualNews;
		expectedNews = new NewsDTO(1001,"Title 1001", "Short text 1001", "Full text 1001"
									, Timestamp.valueOf("2015-11-11 11:38:47.79")
									, Date.valueOf("2015-11-11"));
		actualNews = newsDAO.fetch(1001);
		Assert.assertEquals(expectedNews, actualNews);
	}
	
	@ExpectedDatabase(table="NEWS", value="/testdata/afterUpdateData.xml", assertionMode=DatabaseAssertionMode.NON_STRICT_UNORDERED)
	@Test
	public void testUpdate() throws PersistenceException{
		newsDAO.update(new NewsDTO(1001,"Title 1001", "New short text 1001", "New full text 1001"
				, Timestamp.valueOf("2015-11-11 11:38:47.79")
				, Date.valueOf("2015-11-11")));
	}
	
	@ExpectedDatabase(table="NEWS", value="/testdata/initialData.xml", assertionMode=DatabaseAssertionMode.NON_STRICT_UNORDERED)
	@Test(expected = PersistenceException.class)
	public void testDeleteWithException() throws PersistenceException{
		newsDAO.delete(1002);
	}
	
	@Test
	@ExpectedDatabase(table="NEWS_AUTHORS", value="/testdata/afterInsertData.xml", assertionMode=DatabaseAssertionMode.NON_STRICT_UNORDERED)
	public void testAttachAuthor() throws PersistenceException{
		newsDAO.attachAuthor(1001, 1001);
	}
	
	@Test
	@ExpectedDatabase(table="NEWS_TAGS", value="/testdata/afterInsertData.xml", assertionMode=DatabaseAssertionMode.NON_STRICT_UNORDERED)
	public void testAttachTags() throws PersistenceException{
		newsDAO.attachTags(1001, new ArrayList<Long>(){{add(1001L);add(1002L);}});
	}
	
	@ExpectedDatabase(table="NEWS", value="/testdata/afterDeleteData.xml", assertionMode=DatabaseAssertionMode.NON_STRICT_UNORDERED)
	@Test
	public void testDelete() throws PersistenceException{
		newsDAO.delete(1003);
	}
	
	@Test
	public void testFetchAllNews() throws PersistenceException{
		List<NewsDTO> expectedList = new ArrayList<NewsDTO>();
		expectedList.add(new NewsDTO(1002,"Title 1002", "Short text 1002", "Full text 1002"
										, Timestamp.valueOf("2015-11-11 11:38:47.79")
										, Date.valueOf("2015-11-11")));
		expectedList.add(new NewsDTO(1001,"Title 1001", "Short text 1001", "Full text 1001"
										, Timestamp.valueOf("2015-11-11 11:38:47.79")
										, Date.valueOf("2015-11-11")));
		expectedList.add(new NewsDTO(1003,"Title 1003", "Short text 1003", "Full text 1003"
				, Timestamp.valueOf("2015-11-11 11:38:47.79")
				, Date.valueOf("2015-11-11")));
		List<NewsDTO> actualList = newsDAO.fetchAllNews();
		Assert.assertEquals(expectedList, actualList);
	}
	
	@Test
	public void testGetNewsCount() throws PersistenceException{
		long count = newsDAO.getNewsCount();
		Assert.assertEquals(count, 3);
	}
	
	
	@Test
	public void testSearch() throws PersistenceException{
		SearchCriteria criteria = new SearchCriteria(
				new AuthorDTO(){{setId(1001);}},
				new ArrayList<TagDTO>(){{
					add(new TagDTO(){{setId(1001);}});
					add(new TagDTO(){{setId(1003);}});
				}});
		List<NewsDTO> expectedList = new ArrayList<NewsDTO>();
		expectedList.add(new NewsDTO(1002,"Title 1002", "Short text 1002", "Full text 1002"
										, Timestamp.valueOf("2015-11-11 11:38:47.79")
										, Date.valueOf("2015-11-11")));
		List<NewsDTO> actualList = newsDAO.search(criteria);
		Assert.assertEquals(expectedList, actualList);
	}
	
	
}

package com.epam.newsmanagement.dao.impl;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.dbunit.DatabaseUnitException;
import org.dbunit.assertion.DbUnitAssert;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.epam.newsmanagement.dao.impl.CommentDAOImpl;
import com.epam.newsmanagement.entity.dto.CommentDTO;
import com.epam.newsmanagement.exception.PersistenceException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DbUnitConfiguration;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/test-config.xml"})
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
@DbUnitConfiguration(databaseConnection={"testDatabaseConnection"})
@DatabaseSetup(value="/testdata/initialData.xml")
public class CommentDAOImplTest {
	
	@Autowired
	private CommentDAOImpl commentDAO;
	
	@Autowired
	private IDatabaseConnection databaseConnection;
	
	@Test
	public void testCreate() throws PersistenceException, IOException, SQLException, DatabaseUnitException{
		CommentDTO comment = new CommentDTO();
		comment.setNewsId(1001);
		comment.setCommentText("Comment text 1");
		comment.setCreationDate(Timestamp.valueOf("2015-11-11 11:38:47.79"));
		commentDAO.create(comment);
		
		FlatXmlDataSetBuilder xmlBuilder = new FlatXmlDataSetBuilder();
		InputStream inputStream = ClassLoader.getSystemResourceAsStream("testdata/afterInsertData.xml");
		IDataSet expectedDataset = xmlBuilder.build(new InputStreamReader(inputStream, "UTF-8"));	
		IDataSet actualDataset = databaseConnection.createDataSet();

		(new DbUnitAssert()).assertEqualsIgnoreCols(expectedDataset, actualDataset
				,"COMMENTS", new String[]{"COMMENT_ID"});
	}
	
	@Test
	public void testFetch() throws PersistenceException{
		CommentDTO expectedComment, actualComment;
		expectedComment = new CommentDTO(1001, 1001, "I'm so sutisfied", Timestamp.valueOf("2015-11-11 11:38:47.79"));
		actualComment = commentDAO.fetch(1001);
		Assert.assertEquals(expectedComment, actualComment);
	}
	
	@ExpectedDatabase(table="COMMENTS", value="/testdata/afterUpdateData.xml", assertionMode=DatabaseAssertionMode.NON_STRICT_UNORDERED)
	@Test
	public void testUpdate() throws PersistenceException{
		commentDAO.update(new CommentDTO(1001, 1001, "I'm not impressed", Timestamp.valueOf("2015-11-11 11:38:47.79")));
	}
	
	@ExpectedDatabase(table="COMMENTS", value="/testdata/afterDeleteData.xml", assertionMode=DatabaseAssertionMode.NON_STRICT_UNORDERED)
	@Test
	public void testDelete() throws PersistenceException{
		commentDAO.delete(1001);
	}
	
	@ExpectedDatabase(table="COMMENTS", value="/testdata/afterDeleteData.xml", assertionMode=DatabaseAssertionMode.NON_STRICT_UNORDERED)
	@Test
	public void testDeleteByNewsId() throws PersistenceException{
		commentDAO.deleteByNewsId(1001);
	}
	
	@Test
	public void testFetchOnNewsId() throws PersistenceException{
		List<CommentDTO> expectedComments = new ArrayList<CommentDTO>(), actualComments;
		expectedComments.add(new CommentDTO(1001, 1001, "I'm so sutisfied", Timestamp.valueOf("2015-11-11 11:38:47.79")));
		actualComments = commentDAO.fetchByNewsId(1001);
		Assert.assertEquals(expectedComments, actualComments);
	}
}

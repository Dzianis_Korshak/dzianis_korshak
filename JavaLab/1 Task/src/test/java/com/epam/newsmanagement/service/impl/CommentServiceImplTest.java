package com.epam.newsmanagement.service.impl;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.entity.dto.CommentDTO;
import com.epam.newsmanagement.exception.PersistenceException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.impl.CommentServiceImpl;

import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class CommentServiceImplTest {
	
	private CommentDTO comment = new CommentDTO();
	
	@Mock
	private CommentDAO commentDAO;
	
	@InjectMocks
	private CommentServiceImpl commentService;
	
	@Test
	public void testCreate() throws ServiceException, PersistenceException{
		long actualId, expectedId = 123;
		when(commentDAO.create(comment)).thenReturn(expectedId);
		actualId = commentService.create(comment);
		assertEquals(expectedId, actualId);
	}
	
	@Test(expected=ServiceException.class)
	public void testCreateWithServiceException() throws PersistenceException, ServiceException{
		doThrow(PersistenceException.class).when(commentDAO).create(any(CommentDTO.class));
		commentService.create(comment);
	}
	
	@Test
	public void testFetch() throws ServiceException, PersistenceException{
		when(commentDAO.fetch(anyLong())).thenReturn(comment);
		CommentDTO actualComment = commentService.fetch(anyLong());
		assertEquals(comment, actualComment);
	}
	
	@Test(expected=ServiceException.class)
	public void testFetchWithServiceException() throws ServiceException, PersistenceException{
		doThrow(PersistenceException.class).when(commentDAO).fetch(anyLong());
		commentService.fetch(anyLong());
	}
	
	@Test
	public void testUpdate() throws ServiceException, PersistenceException {
		commentService.update(comment);
		verify(commentDAO).update(comment);
	}
	
	@Test(expected = ServiceException.class)
	public void testUpdateWithServiceException() throws PersistenceException, ServiceException {
		doThrow(PersistenceException.class).when(commentDAO).update(any(CommentDTO.class));
		commentService.update(comment);
	}
	
	@Test
	public void testDelete() throws ServiceException, PersistenceException {
		long commentId = 1L;
		commentService.delete(commentId);
		verify(commentDAO).delete(commentId);
	}
	
	@Test(expected = ServiceException.class)
	public void testDeleteWithServiceException() throws PersistenceException, ServiceException {
		doThrow(PersistenceException.class).when(commentDAO).delete(anyLong());
		commentService.delete(anyLong());
	}
	
	@Test
	public void testDeleteByNewsId() throws PersistenceException, ServiceException {
		long commentId = 1L;
		commentService.deleteByNewsId(commentId);
		verify(commentDAO).deleteByNewsId(commentId);
	}
	
	@Test(expected = ServiceException.class)
	public void testDeleteByNewsIdWithServiceException() throws PersistenceException, ServiceException {
		doThrow(PersistenceException.class).when(commentDAO).deleteByNewsId(anyLong());
		commentService.deleteByNewsId(anyLong());
	}
	
	@Test
	public void testFetchByNewsId() throws ServiceException, PersistenceException{
		List<CommentDTO> expectedComments = new ArrayList<CommentDTO>(), actualComments;
		when(commentDAO.fetchByNewsId(anyLong())).thenReturn(expectedComments);
		actualComments = commentService.fetchByNewsId(anyLong());
		assertEquals(expectedComments, actualComments);
	}
}

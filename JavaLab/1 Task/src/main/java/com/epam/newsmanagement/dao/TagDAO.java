package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.entity.dto.TagDTO;
import com.epam.newsmanagement.exception.PersistenceException;

public interface TagDAO extends CommonDAO<TagDTO> {
	List<TagDTO> fetchByNewsId(long newsId) throws PersistenceException;
}

package com.epam.newsmanagement.service;

import com.epam.newsmanagement.exception.ServiceException;

public interface CommonService<T> {
	
	public long create(T entity) throws ServiceException;
	
	public T fetch(long id) throws ServiceException;
	
	public void update(T entity) throws ServiceException;
	
	public void delete(long id) throws ServiceException;
	
}

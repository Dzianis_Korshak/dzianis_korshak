package com.epam.newsmanagement.util;

import java.util.ArrayList;
import java.util.List;

import com.epam.newsmanagement.entity.dto.AuthorDTO;
import com.epam.newsmanagement.entity.dto.TagDTO;

public class SearchCriteria {
	private AuthorDTO author;
	private List<TagDTO> tagList;
	
	public SearchCriteria(){
		author = new AuthorDTO();
		tagList = new ArrayList<TagDTO>();
	}
	
	public SearchCriteria(AuthorDTO author, List<TagDTO> tagList){
		this.author = author;
		this.tagList = tagList;
	}

	public AuthorDTO getAuthor() {
		return author;
	}

	public void setAuthor(AuthorDTO author) {
		this.author = author;
	}

	public List<TagDTO> getTagList() {
		return tagList;
	}

	public void setTagList(List<TagDTO> tagList) {
		this.tagList = tagList;
	}
	
	public void addTag(TagDTO tag){
		this.tagList.add(tag);
	}
	
	public int getTagsCount(){
		return this.tagList.size();
	}
	
	public TagDTO getTag(int position){
		return tagList.get(position);
	}
}

package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.dto.NewsDTO;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.util.SearchCriteria;

public interface NewsService extends CommonService<NewsDTO> {
	void deleteAuthorReference(long newsId) throws ServiceException;
	void deleteTagsReferences(long newsId) throws ServiceException;
	void deleteNewsComments(long newsId) throws ServiceException;
	List<NewsDTO> fetchAllNews() throws ServiceException;
	long getNewsCount() throws ServiceException;
	void attachAuthor(long newsId, long authorId) throws ServiceException;
	void attachTags(long newsId, List<Long> tagIdList) throws ServiceException;
	List<NewsDTO> search(SearchCriteria criteria) throws ServiceException;
}

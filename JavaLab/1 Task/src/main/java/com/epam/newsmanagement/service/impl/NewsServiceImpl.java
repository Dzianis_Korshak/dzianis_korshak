package com.epam.newsmanagement.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.entity.dto.NewsDTO;
import com.epam.newsmanagement.exception.PersistenceException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.util.SearchCriteria;

@Service("newsService")
public class NewsServiceImpl implements NewsService {
	
	private static final Logger LOG = Logger.getLogger(NewsServiceImpl.class);
	
	@Autowired
	private NewsDAO newsDAO;
	
	public long create(NewsDTO entity) throws ServiceException {
		try{
			return newsDAO.create(entity);
		} catch(PersistenceException ex){
			LOG.error(ex.getMessage(),ex);
			throw new ServiceException(ex);
		}
	}

	public NewsDTO fetch(long id) throws ServiceException {
		try{
			return newsDAO.fetch(id);
		}catch(PersistenceException ex){
			LOG.error(ex.getMessage(),ex);
			throw new ServiceException(ex);
		}
	}

	public void update(NewsDTO entity) throws ServiceException {
		try{
			newsDAO.update(entity);
		} catch(PersistenceException ex){
			LOG.error(ex.getMessage(),ex);
			throw new ServiceException(ex);
		}
	}

	public void delete(long id) throws ServiceException {
		try{
			newsDAO.delete(id);
		} catch(PersistenceException ex){
			LOG.error(ex.getMessage(),ex);
			throw new ServiceException(ex);
		}
	}

	public void deleteAuthorReference(long newsId) throws ServiceException {
		try{
			newsDAO.deleteAuthorReference(newsId);
		} catch(PersistenceException ex){
			LOG.error(ex.getMessage(),ex);
			throw new ServiceException(ex);
		}
	}

	public void deleteTagsReferences(long newsId) throws ServiceException {
		try{
			newsDAO.deleteTagsReferences(newsId);
		} catch(PersistenceException ex){
			LOG.error(ex.getMessage(),ex);
			throw new ServiceException(ex);
		}
	}

	public void deleteNewsComments(long newsId) throws ServiceException {
		try{
			newsDAO.deleteNewsComments(newsId);
		} catch(PersistenceException ex){
			LOG.error(ex.getMessage(),ex);
			throw new ServiceException(ex);
		}
	}

	public List<NewsDTO> fetchAllNews() throws ServiceException {
		try{
			return newsDAO.fetchAllNews();
		} catch(PersistenceException ex){
			LOG.error(ex.getMessage(),ex);
			throw new ServiceException(ex);
		}
	}

	public long getNewsCount() throws ServiceException {
		try{
			return newsDAO.getNewsCount();
		} catch(PersistenceException ex){
			LOG.error(ex.getMessage(),ex);
			throw new ServiceException(ex);
		}
	}

	public void attachAuthor(long newsId, long authorId) throws ServiceException {
		try{
			newsDAO.attachAuthor(newsId, authorId);
		} catch(PersistenceException ex){
			LOG.error(ex.getMessage(),ex);
			throw new ServiceException(ex);
		}
	}

	public void attachTags(long newsId, List<Long> tagIdList) throws ServiceException {
		try{
			newsDAO.attachTags(newsId, tagIdList);
		} catch(PersistenceException ex){
			LOG.error(ex.getMessage(),ex);
			throw new ServiceException(ex);
		}
	}

	public List<NewsDTO> search(SearchCriteria criteria) throws ServiceException {
		try{
			return newsDAO.search(criteria);
		}catch(PersistenceException ex){
			LOG.error(ex.getMessage(),ex);
			throw new ServiceException(ex);
		}
	}

}

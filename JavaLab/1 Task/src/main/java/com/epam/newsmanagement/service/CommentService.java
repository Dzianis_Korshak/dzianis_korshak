package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.dto.CommentDTO;
import com.epam.newsmanagement.exception.ServiceException;

public interface CommentService extends CommonService<CommentDTO> {
	
	void deleteByNewsId(long newsId) throws ServiceException;
	List<CommentDTO> fetchByNewsId(long newsId) throws ServiceException;
}

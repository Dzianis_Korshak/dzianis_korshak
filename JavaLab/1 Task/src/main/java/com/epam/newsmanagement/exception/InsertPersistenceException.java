package com.epam.newsmanagement.exception;

public class InsertPersistenceException extends PersistenceException {

	public InsertPersistenceException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public InsertPersistenceException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public InsertPersistenceException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public InsertPersistenceException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public InsertPersistenceException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}
	
}

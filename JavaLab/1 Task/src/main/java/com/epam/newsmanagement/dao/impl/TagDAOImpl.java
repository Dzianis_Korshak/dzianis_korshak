package com.epam.newsmanagement.dao.impl;

import static com.epam.newsmanagement.dao.util.DatabaseUtil.closeResources;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.entity.dto.TagDTO;
import com.epam.newsmanagement.exception.InsertPersistenceException;
import com.epam.newsmanagement.exception.PersistenceException;

@Repository("tagDAO")
public class TagDAOImpl implements TagDAO {
	
	private static final String SQL_CREATE_TAG = "INSERT INTO TAGS(TAG_ID, TAG_NAME)"
				+ " VALUES(tags_sequence.nextval, ?)";
	
	private static final String SQL_FETCH_TAG = "SELECT tag_id, tag_name from TAGS WHERE tag_id = ?";
	
	private static final String SQL_UPDATE_TAG = "UPDATE TAGS SET tag_name = ? where tag_id = ?";
	
	private static final String SQL_DELETE_TAG = "DELETE FROM TAGS WHERE tag_id = ?";
	
	private static final String SQL_FETCH_TAG_BY_NEWS_ID
				= "SELECT t.tag_id, t.tag_name from TAGS t left join NEWS_TAGS nt"
				+ " on t.tag_id = nt.tag_id where news_id = ?";
	
	@Autowired
	private DataSource dataSource;
	
	public long create(TagDTO entity) throws PersistenceException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try{
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(SQL_CREATE_TAG, new String[]{"tag_id"});
			preparedStatement.setString(1, entity.getTagName());
			preparedStatement.executeUpdate();
			resultSet = preparedStatement.getGeneratedKeys();
			if(resultSet.next()){
				return resultSet.getLong(1);
			} else {
				throw new InsertPersistenceException();
			}
		} catch(SQLException ex){
			throw new PersistenceException(ex);
		} finally {
			closeResources(connection, preparedStatement);
		}
	}

	public TagDTO fetch(long id) throws PersistenceException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try{
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(SQL_FETCH_TAG);
			preparedStatement.setLong(1, id);
			resultSet = preparedStatement.executeQuery();
			TagDTO tag = null;
			if(resultSet.next()){
				tag = getTagFromResultSet(resultSet);
			}
			if(tag == null){
				throw new PersistenceException("No such author fetched with id = " + id);
			}
			return tag;
		} catch(SQLException ex){
			throw new PersistenceException(ex);
		} finally {
			closeResources(connection, preparedStatement, resultSet);
		}
	}

	public void update(TagDTO entity) throws PersistenceException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try{
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(SQL_UPDATE_TAG);
			preparedStatement.setString(1, entity.getTagName());
			preparedStatement.setLong(2, entity.getId());
			preparedStatement.executeUpdate();
		} catch(SQLException ex){
			throw new PersistenceException(ex);
		} finally {
			closeResources(connection, preparedStatement);
		}
	}

	public void delete(long id) throws PersistenceException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try{
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(SQL_DELETE_TAG);
			preparedStatement.setLong(1, id);
			preparedStatement.executeUpdate();
		} catch(SQLException ex){
			throw new PersistenceException(ex);
		} finally {
			closeResources(connection, preparedStatement);
		}
	}
	
	private TagDTO getTagFromResultSet(ResultSet resultSet) throws SQLException{
		TagDTO tag= null;
		tag = new TagDTO();
		tag.setId(resultSet.getLong(1));
		tag.setTagName(resultSet.getString(2));
		return tag;
	}

	public List<TagDTO> fetchByNewsId(long newsId) throws PersistenceException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<TagDTO> tags = new ArrayList<TagDTO>();
		try{
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(SQL_FETCH_TAG_BY_NEWS_ID);
			preparedStatement.setLong(1, newsId);
			resultSet = preparedStatement.executeQuery();
			while(resultSet.next()){
				tags.add(getTagFromResultSet(resultSet));
			}
			return tags;
		} catch(SQLException ex){
			throw new PersistenceException(ex);
		} finally {
			closeResources(connection, preparedStatement, resultSet);
		}
	}	
}

package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.dto.TagDTO;
import com.epam.newsmanagement.exception.ServiceException;

public interface TagService extends CommonService<TagDTO> {
	List<TagDTO> fetchByNewsId(long newsId) throws ServiceException;
}

package com.epam.newsmanagement.dao.impl;

import static com.epam.newsmanagement.dao.util.DatabaseUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.entity.dto.AuthorDTO;
import com.epam.newsmanagement.exception.InsertPersistenceException;
import com.epam.newsmanagement.exception.PersistenceException;

@Repository("authorDAO")
public class AuthorDAOImpl implements AuthorDAO {
		
	private static final String SQL_CREATE_AUTHOR = "INSERT INTO AUTHORS(author_id, author_name, expired)"
									+ " VALUES(authors_sequence.nextval, ?, ?)";
	
	private static final String SQL_SELECT_AUTHOR = "SELECT * FROM authors WHERE author_id = ?";
	
	private static final String SQL_UPDATE_AUTHOR = "UPDATE authors SET author_name = ?, expired = ? where author_id = ?";
	
	private static final String SQL_DELETE_AUTHOR = "DELETE FROM authors WHERE author_id = ?";
	
	private static final String SQL_FETCH_AUTHOR_BY_NEWS 
				= "SELECT a.author_id, a.author_name, a.expired"
				+ " from AUTHORS a left join NEWS_AUTHORS na on a.author_id = na.author_id where na.news_id = ?";
	
	@Autowired
	private DataSource dataSource;
		
	public AuthorDAOImpl() {
		super();
	}

	public long create(AuthorDTO entity) throws PersistenceException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try{
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(SQL_CREATE_AUTHOR, new String[]{"author_id"});
			preparedStatement.setString(1, entity.getName());
			if(entity.getExpired() != null){
				preparedStatement.setTimestamp(2, entity.getExpired());
			} else {
				preparedStatement.setNull(2, java.sql.Types.TIMESTAMP);
			}
			preparedStatement.executeUpdate();
			resultSet = preparedStatement.getGeneratedKeys();
			if(resultSet.next()){
				return resultSet.getLong(1);
			} else {
				throw new InsertPersistenceException();
			}
		} catch(SQLException ex){
			throw new PersistenceException(ex);
		} finally {
			closeResources(connection, preparedStatement);
		}
	}

	public AuthorDTO fetch(long id) throws PersistenceException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try{
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(SQL_SELECT_AUTHOR);
			preparedStatement.setLong(1, id);
			resultSet = preparedStatement.executeQuery();
			AuthorDTO author = getAuthorFromResultSet(resultSet);
			if(author == null){
				throw new PersistenceException("No such author fetched with id = " + id);
			}
			return author;
		} catch(SQLException ex){
			throw new PersistenceException(ex);
		} finally {
			closeResources(connection, preparedStatement, resultSet);
		}
	}

	public void update(AuthorDTO entity) throws PersistenceException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try{
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(SQL_UPDATE_AUTHOR);
			preparedStatement.setString(1, entity.getName());
			preparedStatement.setTimestamp(2, entity.getExpired());
			preparedStatement.setLong(3, entity.getId());
			preparedStatement.executeUpdate();
		} catch(SQLException ex){
			throw new PersistenceException(ex);
		} finally {
			closeResources(connection, preparedStatement);
		}
	}

	public void delete(long id) throws PersistenceException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try{
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(SQL_DELETE_AUTHOR);
			preparedStatement.setLong(1, id);
			preparedStatement.executeUpdate();
		} catch(SQLException ex){
			throw new PersistenceException(ex);
		} finally {
			closeResources(connection, preparedStatement);
		}
	}
	
	private AuthorDTO getAuthorFromResultSet(ResultSet resultSet) throws SQLException{
		AuthorDTO author = null;
		if(resultSet.next()){
			author = new AuthorDTO();
			author.setId(resultSet.getLong(1));
			author.setName(resultSet.getString(2));
			author.setExpired(resultSet.getTimestamp(3));
		}
		return author;
	}

	public AuthorDTO fetchByNewsId(long newsId) throws PersistenceException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try{
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(SQL_FETCH_AUTHOR_BY_NEWS);
			preparedStatement.setLong(1, newsId);
			resultSet = preparedStatement.executeQuery();
			AuthorDTO author = getAuthorFromResultSet(resultSet);
			if(author == null){
				throw new PersistenceException("No such author fetched with id = " + newsId);
			}
			return author;
		} catch(SQLException ex){
			throw new PersistenceException(ex);
		} finally {
			closeResources(connection, preparedStatement, resultSet);
		}
	}
}

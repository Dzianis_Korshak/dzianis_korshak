package com.epam.newsmanagement.dao.util;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.epam.newsmanagement.exception.PersistenceException;

public class DatabaseUtil {
	/**
	 * Close resources.
	 *
	 * @param connection the connection
	 * @param statement the statement
	 * @param resultSet the result set
	 */
	public static void closeResources(Connection connection, Statement statement,
								ResultSet resultSet){
		try {
			closeResultSet(resultSet);
		} catch (SQLException e) {
		}
		try {
			closeStatement(statement);
		} catch (SQLException e) {
		}
		try {
			closeConnection(connection);
		} catch (SQLException e) {
		}
	}
	
	/**
	 * Close resources.
	 *
	 * @param connection the connection
	 * @param statement the statement
	 * @throws DAOException when you have some troubles with 
	 * closing resources
	 */	
	public static void closeResources(Connection connection, Statement statement) throws PersistenceException {
		
		try {
			closeStatement(statement);
		} catch (SQLException e1) {
			throw new PersistenceException("Problem with closing Statement", e1);
		}
		
		try {
			closeConnection(connection);
		} catch (SQLException e) {
			throw new PersistenceException("Problem with closing Connection", e);
		}
	}
	
	/**
	 * Close connection.
	 *
	 * @param connection the connection that is used in DAO.
	 * @throws SQLException when you have some troubles with
	 * closing the connection with Database
	 */
	private static void closeConnection(Connection connection) throws SQLException {
		if(connection != null) {
			connection.close();
		}
	}
	
	/**
	 * Close statement.
	 *
	 * @param statement the statement that is used in DAO.
	 * @throws SQLException when you have some troubles with
	 *  closing the statement
	 */
	private static void closeStatement(Statement statement) throws SQLException {
		if(statement != null) {
			statement.close();
		}
	}
	
	/**
	 * Close result set.
	 *
	 * @param resultSet the resultSet  that is used in DAO.
	 * @throws SQLException when you have some troubles with
	 * closing the resultSet
	 */
	private static void closeResultSet(ResultSet resultSet) throws SQLException {
		if(resultSet != null) {
			resultSet.close();
		}
	}
}

package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.dto.NewsDTO;
import com.epam.newsmanagement.exception.ServiceException;

public interface NewsManager {

	long saveNews(NewsDTO newsDTO, long authorId, List<Long> tagIdList) throws ServiceException;
	News getNews(long newsId) throws ServiceException;
}

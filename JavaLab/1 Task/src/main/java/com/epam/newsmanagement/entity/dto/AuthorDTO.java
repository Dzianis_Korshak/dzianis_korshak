package com.epam.newsmanagement.entity.dto;

import java.io.Serializable;
import java.sql.Timestamp;

public class AuthorDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 18039929164759776L;
	
	private long id;
	
	private String name;
	
	private Timestamp expired;

	public AuthorDTO(long id, String name, Timestamp expired) {
		super();
		this.id = id;
		this.name = name;
		this.expired = expired;
	}

	public AuthorDTO(long id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	public AuthorDTO(long id) {
		super();
		this.id = id;
	}

	public AuthorDTO() {
		super();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Timestamp getExpired() {
		return expired;
	}

	public void setExpired(Timestamp expired) {
		this.expired = expired;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((expired == null) ? 0 : expired.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AuthorDTO other = (AuthorDTO) obj;
		if (expired == null) {
			if (other.expired != null)
				return false;
		} else if (!expired.equals(other.expired))
			return false;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "AuthorDTO [id=" + id + ", name=" + name + ", expired=" + expired + "]";
	}
	
	
}

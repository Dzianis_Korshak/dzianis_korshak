package com.epam.newsmanagement.exception;

public class DateParseException extends PersistenceException {

	public DateParseException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public DateParseException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public DateParseException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public DateParseException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public DateParseException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}
	
}

package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.entity.dto.CommentDTO;
import com.epam.newsmanagement.exception.PersistenceException;

public interface CommentDAO extends CommonDAO<CommentDTO> {
	void deleteByNewsId(long newsId) throws PersistenceException;
	List<CommentDTO> fetchByNewsId(long newsId) throws PersistenceException;
}

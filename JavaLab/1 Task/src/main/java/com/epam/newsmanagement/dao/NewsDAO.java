package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.entity.dto.NewsDTO;
import com.epam.newsmanagement.exception.PersistenceException;
import com.epam.newsmanagement.util.SearchCriteria;

public interface NewsDAO extends CommonDAO<NewsDTO> {
	void deleteAuthorReference(long newsId) throws PersistenceException;
	void deleteTagsReferences(long newsId) throws PersistenceException;
	void deleteNewsComments(long newsId) throws PersistenceException;
	List<NewsDTO> fetchAllNews() throws PersistenceException;
	long getNewsCount() throws PersistenceException;
	void attachAuthor(long newsId, long authorId) throws PersistenceException;
	void attachTags(long newsId, List<Long> tagIdList) throws PersistenceException;
	List<NewsDTO> search(SearchCriteria criteria) throws PersistenceException;
}

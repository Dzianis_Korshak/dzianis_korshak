INSERT INTO authors(author_id, author_name) VALUES(authors_sequence.nextval, 'John Smith');
INSERT INTO authors(author_id, author_name) VALUES(authors_sequence.nextval, 'Peter Gryffin');
INSERT INTO authors(author_id, author_name) VALUES(authors_sequence.nextval, 'Samuel Peter');
INSERT INTO authors(author_id, author_name) VALUES(authors_sequence.nextval, 'Vitalyi Klichko');
INSERT INTO authors(author_id, author_name) VALUES(authors_sequence.nextval, 'John Johns');
INSERT INTO authors(author_id, author_name) VALUES(authors_sequence.nextval, 'Denis Shmenis');
INSERT INTO authors(author_id, author_name) VALUES(authors_sequence.nextval, 'Ronda Rousy');
INSERT INTO authors(author_id, author_name) VALUES(authors_sequence.nextval, 'David Becham');
INSERT INTO authors(author_id, author_name) VALUES(authors_sequence.nextval, 'Poul Smith');
INSERT INTO authors(author_id, author_name) VALUES(authors_sequence.nextval, 'Cris Markis');
INSERT INTO authors(author_id, author_name) VALUES(authors_sequence.nextval, 'Kate Plum');
INSERT INTO authors(author_id, author_name) VALUES(authors_sequence.nextval, 'Marry Smarry');
INSERT INTO authors(author_id, author_name) VALUES(authors_sequence.nextval, 'Nick Travolta');
INSERT INTO authors(author_id, author_name) VALUES(authors_sequence.nextval, 'Stan Marsh');
INSERT INTO authors(author_id, author_name) VALUES(authors_sequence.nextval, 'Eric Cartman');
INSERT INTO authors(author_id, author_name) VALUES(authors_sequence.nextval, 'Kayle Brathlowski');
INSERT INTO authors(author_id, author_name) VALUES(authors_sequence.nextval, 'David Trump');
INSERT INTO authors(author_id, author_name) VALUES(authors_sequence.nextval, 'Mikle Perez');
INSERT INTO authors(author_id, author_name) VALUES(authors_sequence.nextval, 'John Bowl');
INSERT INTO authors(author_id, author_name) VALUES(authors_sequence.nextval, 'Steven Lobs');

INSERT INTO news(news_id, title, short_text, full_text, creation_date, modification_date)
  VALUES(news_sequence.nextval
    , 'Bomb Downed Plane in Egypt'
    , 'Moscow confirming for the first time on Tuesday that a bomb brought down a Russian charter jet'
    , 'MOSCOW � Hours after confirming for the first time on Tuesday that a bomb brought down a Russian charter jet over the Sinai Peninsula in Egypt more than two weeks ago, killing all 224 people aboard, Russia joined France in bombing Islamic State targets in Syria.
      For a second straight day French warplanes hit a command post and a recruitment center for jihadists in an Islamic State stronghold, Raqqa, the French Ministry of Defense announced on its website, while Russian news reports said a Russian submarine had fired cruise missiles at Islamic State targets in the same area.'
    , SYSTIMESTAMP, SYSDATE);
INSERT INTO news(news_id, title, short_text, full_text, creation_date, modification_date)
  VALUES(news_sequence.nextval
    , 'Charlie Sheen on Today'
    , 'Charlie Sheen broke his silence on his health issues on NBC Today on Tuesday'
    , 'I�m here to admit I am in fact HIV positive, Sheen said. �I have to put a stop to this onslaught, this barrage of attacks, of sub-truths. Very harmful stories that are threatening the health of so many others. They couldn�t be farther from the truth.�
      The diagnosis arrived four years ago, Sheen revealed. �It started with what I thought was a series of cluster headaches. Sweating the bed. I was hospitalized. I thought I had a brain tumor. I thought it was over � It�s a hard three letters to absorb. It�s a turning point in one�s life.� He claims he doesn�t know how he contracted the virus. '
    , SYSTIMESTAMP, SYSDATE);
/*
TODO: more 12 rows
*/

INSERT INTO comments(comment_id, news_id, comment_text, creation_date)
  VALUES(comments_sequence.nextval, 1, 'It is a pity', SYSDATE);
INSERT INTO comments(comment_id, news_id, comment_text, creation_date)
  VALUES(comments_sequence.nextval, 1, 'Sad thing', SYSDATE);
INSERT INTO comments(comment_id, news_id, comment_text, creation_date)
  VALUES(comments_sequence.nextval, 1, 'It is a shame', SYSDATE);
INSERT INTO comments(comment_id, news_id, comment_text, creation_date)
  VALUES(comments_sequence.nextval, 2, 'Nice joke!', SYSDATE);
INSERT INTO comments(comment_id, news_id, comment_text, creation_date)
  VALUES(comments_sequence.nextval, 2, 'No way!', SYSDATE);
/*
TODO: more 15 comments
*/

INSERT INTO news_authors(news_id, author_id) VALUES(1,1);
INSERT INTO news_authors(news_id, author_id) VALUES(2,2);
/*
TODO: more 18 rows
*/

INSERT INTO tags(tag_id, tag_name) VALUES(tags_sequence.nextval, 'brand new');
INSERT INTO tags(tag_id, tag_name) VALUES(tags_sequence.nextval, 'today');
INSERT INTO tags(tag_id, tag_name) VALUES(tags_sequence.nextval, 'new year');
INSERT INTO tags(tag_id, tag_name) VALUES(tags_sequence.nextval, 'artwork');
INSERT INTO tags(tag_id, tag_name) VALUES(tags_sequence.nextval, 'live');
INSERT INTO tags(tag_id, tag_name) VALUES(tags_sequence.nextval, 'fun');
INSERT INTO tags(tag_id, tag_name) VALUES(tags_sequence.nextval, 'sport');
INSERT INTO tags(tag_id, tag_name) VALUES(tags_sequence.nextval, 'love');
INSERT INTO tags(tag_id, tag_name) VALUES(tags_sequence.nextval, 'video');
INSERT INTO tags(tag_id, tag_name) VALUES(tags_sequence.nextval, 'family');
INSERT INTO tags(tag_id, tag_name) VALUES(tags_sequence.nextval, 'budget');
INSERT INTO tags(tag_id, tag_name) VALUES(tags_sequence.nextval, 'home');
INSERT INTO tags(tag_id, tag_name) VALUES(tags_sequence.nextval, 'pet');
INSERT INTO tags(tag_id, tag_name) VALUES(tags_sequence.nextval, 'space');
INSERT INTO tags(tag_id, tag_name) VALUES(tags_sequence.nextval, 'terror');
INSERT INTO tags(tag_id, tag_name) VALUES(tags_sequence.nextval, 'bbc');
INSERT INTO tags(tag_id, tag_name) VALUES(tags_sequence.nextval, 'blur');
INSERT INTO tags(tag_id, tag_name) VALUES(tags_sequence.nextval, 'new');
INSERT INTO tags(tag_id, tag_name) VALUES(tags_sequence.nextval, 'photos');
INSERT INTO tags(tag_id, tag_name) VALUES(tags_sequence.nextval, 'conan');

INSERT INTO news_tags(news_id, tag_id) VALUES(1,1);
INSERT INTO news_tags(news_id, tag_id) VALUES(1,2);
INSERT INTO news_tags(news_id, tag_id) VALUES(1,15);
INSERT INTO news_tags(news_id, tag_id) VALUES(2,2);
INSERT INTO news_tags(news_id, tag_id) VALUES(2,9);
/*
TODO: more 15 rows
*/



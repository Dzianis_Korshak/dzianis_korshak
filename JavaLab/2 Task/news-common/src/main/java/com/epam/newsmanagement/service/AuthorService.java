package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.dto.AuthorDTO;
import com.epam.newsmanagement.exception.ServiceException;

public interface AuthorService extends CommonService<AuthorDTO> {
	AuthorDTO fetchByNewsId(long newsId) throws ServiceException;
	List<AuthorDTO> fetchAll() throws ServiceException;
}

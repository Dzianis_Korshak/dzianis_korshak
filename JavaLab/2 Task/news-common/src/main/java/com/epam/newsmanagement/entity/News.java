package com.epam.newsmanagement.entity;

import java.util.List;

import com.epam.newsmanagement.entity.dto.AuthorDTO;
import com.epam.newsmanagement.entity.dto.CommentDTO;
import com.epam.newsmanagement.entity.dto.NewsDTO;
import com.epam.newsmanagement.entity.dto.TagDTO;

public class News {

	private NewsDTO news;
	private AuthorDTO author;
	private List<TagDTO> tagList;
	private List<CommentDTO> commentList;
	
	public News(){}
	
	public NewsDTO getNews() {
		return news;
	}
	public void setNews(NewsDTO news) {
		this.news = news;
	}
	public AuthorDTO getAuthor() {
		return author;
	}
	public void setAuthor(AuthorDTO author) {
		this.author = author;
	}
	public List<TagDTO> getTagList() {
		return tagList;
	}
	public void setTagList(List<TagDTO> tagList) {
		this.tagList = tagList;
	}
	public List<CommentDTO> getCommentList() {
		return commentList;
	}
	public void setCommentList(List<CommentDTO> commentList) {
		this.commentList = commentList;
	}
	
}

package com.epam.newsmanagement.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.entity.dto.AuthorDTO;
import com.epam.newsmanagement.exception.PersistenceException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorService;

@Service("authorService")
public class AuthorServiceImpl implements AuthorService {
	
	private static final Logger LOG = Logger.getLogger(AuthorServiceImpl.class);
	
	@Autowired
	private AuthorDAO authorDAO;
	
	public long create(AuthorDTO entity) throws ServiceException {
		try{
			return authorDAO.create(entity);
		} catch(PersistenceException ex){
			LOG.error(ex.getMessage(),ex);
			throw new ServiceException(ex);
		}
	}

	public AuthorDTO fetch(long id) throws ServiceException {
		try{
			return authorDAO.fetch(id);
		} catch(PersistenceException ex){
			LOG.error(ex.getMessage(),ex);
			throw new ServiceException(ex);
		}
	}

	public void update(AuthorDTO entity) throws ServiceException {
		try{
			authorDAO.update(entity);
		} catch(PersistenceException ex){
			LOG.error(ex.getMessage(),ex);
			throw new ServiceException(ex);
		}
	}

	public void delete(long id) throws ServiceException {
		try{
			authorDAO.delete(id);
		} catch(PersistenceException ex){
			LOG.error(ex.getMessage(),ex);
			throw new ServiceException(ex);
		}
	}

	public AuthorDTO fetchByNewsId(long newsId) throws ServiceException {
		try{
			return authorDAO.fetchByNewsId(newsId);
		} catch(PersistenceException ex){
			LOG.error(ex.getMessage(),ex);
			throw new ServiceException(ex);
		}
	}

	public List<AuthorDTO> fetchAll() throws ServiceException {
		try{
			return authorDAO.fetchAll();
		} catch(PersistenceException ex){
			LOG.error(ex.getMessage(),ex);
			throw new ServiceException(ex);
		}
	}

}

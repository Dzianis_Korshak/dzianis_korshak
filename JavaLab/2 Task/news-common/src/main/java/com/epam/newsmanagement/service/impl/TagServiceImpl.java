package com.epam.newsmanagement.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.entity.dto.TagDTO;
import com.epam.newsmanagement.exception.PersistenceException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.TagService;

@Service("tagService")
public class TagServiceImpl implements TagService {

	private static final Logger LOG = Logger.getLogger(TagServiceImpl.class);
	
	@Autowired
	private TagDAO tagDAO;
	
	public long create(TagDTO entity) throws ServiceException {
		try{
			return tagDAO.create(entity);
		} catch(PersistenceException ex){
			LOG.error(ex.getMessage(),ex);
			throw new ServiceException(ex);
		}
	}

	public TagDTO fetch(long id) throws ServiceException {
		try{
			return tagDAO.fetch(id);
		} catch(PersistenceException ex){
			LOG.error(ex.getMessage(),ex);
			throw new ServiceException(ex);
		}
	}

	public void update(TagDTO entity) throws ServiceException {
		try{
			tagDAO.update(entity);
		} catch(PersistenceException ex){
			LOG.error(ex.getMessage(),ex);
			throw new ServiceException(ex);
		}
	}

	public void delete(long id) throws ServiceException {
		try{
			tagDAO.delete(id);
		} catch(PersistenceException ex){
			LOG.error(ex.getMessage(),ex);
			throw new ServiceException(ex);
		}
	}

	public List<TagDTO> fetchByNewsId(long newsId) throws ServiceException {
		try{
			return tagDAO.fetchByNewsId(newsId);
		} catch(PersistenceException ex){
			LOG.error(ex.getMessage(),ex);
			throw new ServiceException(ex);
		}
	}

	public List<TagDTO> fetchAll() throws ServiceException {
		try{
			return tagDAO.fetchAll();
		} catch(PersistenceException ex){
			LOG.error(ex.getMessage(),ex);
			throw new ServiceException(ex);
		}
	}
	
	

}

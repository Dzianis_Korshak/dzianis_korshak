package com.epam.newsmanagement.exception;

public class DateParseException extends PersistenceException {

	private static final long serialVersionUID = 4846140909797584120L;

	public DateParseException() {
		super();
	}

	public DateParseException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public DateParseException(String message, Throwable cause) {
		super(message, cause);
	}

	public DateParseException(String message) {
		super(message);
	}

	public DateParseException(Throwable cause) {
		super(cause);
	}
	
}

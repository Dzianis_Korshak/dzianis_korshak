package com.epam.newsmanagement.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.dto.AuthorDTO;
import com.epam.newsmanagement.entity.dto.CommentDTO;
import com.epam.newsmanagement.entity.dto.NewsDTO;
import com.epam.newsmanagement.entity.dto.TagDTO;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.CommentService;
import com.epam.newsmanagement.service.NewsManager;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.service.TagService;
import com.epam.newsmanagement.util.SearchCriteria;

@Service("newsManager")
public class NewsManagerImpl implements NewsManager {

	@Autowired
	private NewsService newsService;
	
	@Autowired
	private AuthorService authorService;
	
	@Autowired
	private TagService tagService;
	
	@Autowired
	private CommentService commentService;
	
	@Transactional(rollbackFor=Exception.class)
	public long saveNews(NewsDTO newsDTO, long authorId, List<Long> tagIdList) throws ServiceException {
		long newsId = newsService.create(newsDTO);
		newsService.attachAuthor(newsId, authorId);
		newsService.attachTags(newsId, tagIdList);
		return newsId;
	}
	
	@Transactional(rollbackFor=Exception.class)
	public News getNews(long newsId) throws ServiceException{
		News news = new News();
		news.setNews(newsService.fetch(newsId));
		news.setAuthor(authorService.fetchByNewsId(newsId));
		news.setCommentList(commentService.fetchByNewsId(newsId));
		news.setTagList(tagService.fetchByNewsId(newsId));
		return news;
	}
	
	@Transactional(rollbackFor=Exception.class)
	public List<News> search(SearchCriteria criteria, int offset, int number) throws ServiceException{
		List<News> newsList = new ArrayList<News>();
		for(NewsDTO newsDTO : newsService.search(criteria, offset, number)){
			News news = new News();
			news.setNews(newsDTO);
			news.setAuthor(authorService.fetchByNewsId(newsDTO.getId()));
			news.setCommentList(commentService.fetchByNewsId(newsDTO.getId()));
			news.setTagList(tagService.fetchByNewsId(newsDTO.getId()));
			newsList.add(news);
		}
		return newsList;
	}

	public List<AuthorDTO> getAuthors() throws ServiceException {
		return authorService.fetchAll();
	}

	public List<TagDTO> getTags() throws ServiceException {
		return tagService.fetchAll();
	}
	
	public long getNewsNumber() throws ServiceException{
		return newsService.getNewsNumber();
	}

	public long getNewsNumber(SearchCriteria searchCriteria) throws ServiceException {
		return newsService.getNewsNumber(searchCriteria);
	}

	public long getNewsPosition(SearchCriteria searchCriteria, long newsId) throws ServiceException {	
		return newsService.getNewsPosition(searchCriteria, newsId);
	}
	
	@Transactional(rollbackFor=Exception.class)
	public List<News> getNewsTripplet(SearchCriteria criteria, long newsId) throws ServiceException{
		List<News> newsList = new ArrayList<News>();
		for(NewsDTO newsDTO : newsService.getNewsTripplet(criteria, newsId)){
			if(newsDTO != null){
				News news = new News();
				news.setNews(newsDTO);
				news.setAuthor(authorService.fetchByNewsId(newsDTO.getId()));
				news.setCommentList(commentService.fetchByNewsId(newsDTO.getId()));
				news.setTagList(tagService.fetchByNewsId(newsDTO.getId()));
				newsList.add(news);
			} else {
				newsList.add(null);
			}
		}
		return newsList;
	}

	public long addComment(CommentDTO comment) throws ServiceException {
		return commentService.create(comment);
	}

	public void deleteComment(long commentId) throws ServiceException {
		commentService.delete(commentId);		
	}
}

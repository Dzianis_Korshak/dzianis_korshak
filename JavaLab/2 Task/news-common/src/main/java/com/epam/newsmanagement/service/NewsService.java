package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.dto.NewsDTO;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.util.SearchCriteria;

public interface NewsService extends CommonService<NewsDTO> {
	void deleteAuthorReference(long newsId) throws ServiceException;
	void deleteTagsReferences(long newsId) throws ServiceException;
	void deleteNewsComments(long newsId) throws ServiceException;
	List<NewsDTO> fetchAllNews() throws ServiceException;
	long getNewsNumber() throws ServiceException;
	long getNewsNumber(SearchCriteria searchCriteria) throws ServiceException;
	long getNewsPosition(SearchCriteria searchCriteria, long newsId) throws ServiceException;
	void attachAuthor(long newsId, long authorId) throws ServiceException;
	void attachTags(long newsId, List<Long> tagIdList) throws ServiceException;
	List<NewsDTO> search(SearchCriteria criteria, long offset, long number) throws ServiceException;
	List<NewsDTO> getNewsTripplet(SearchCriteria criteria, long newsId) throws ServiceException;
}

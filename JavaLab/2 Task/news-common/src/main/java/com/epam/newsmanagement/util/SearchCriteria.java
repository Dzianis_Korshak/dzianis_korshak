package com.epam.newsmanagement.util;

import java.util.ArrayList;
import java.util.List;

public class SearchCriteria {
	private long authorId;
	private List<Long> tagList;
	
	public SearchCriteria(){
		tagList = new ArrayList<Long>();
	}
	
	public SearchCriteria(long authorId, List<Long> tagList){
		this.authorId = authorId;
		this.tagList = tagList;
	}

	public long getAuthorId() {
		return authorId;
	}

	public void setAuthorId(long authorId) {
		this.authorId = authorId;
	}

	public List<Long> getTagIdList() {
		return tagList;
	}

	public void setTagIdList(List<Long> tagList) {
		if(tagList == null){
			this.tagList.clear();
		} else {
			this.tagList = tagList;
		}
	}
	
	public void addTagId(Long tag){
		this.tagList.add(tag);
	}
	
	public int getTagsCount(){
		return this.tagList.size();
	}
	
	public Long getTagId(int position){
		return tagList.get(position);
	}
	
	public boolean contains(Long tagId){
		return tagList.contains(tagId);
	}
}

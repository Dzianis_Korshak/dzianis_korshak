package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.dto.AuthorDTO;
import com.epam.newsmanagement.entity.dto.CommentDTO;
import com.epam.newsmanagement.entity.dto.NewsDTO;
import com.epam.newsmanagement.entity.dto.TagDTO;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.util.SearchCriteria;

public interface NewsManager {

	long saveNews(NewsDTO newsDTO, long authorId, List<Long> tagIdList) throws ServiceException;
	News getNews(long newsId) throws ServiceException;
	long getNewsNumber() throws ServiceException;
	long getNewsNumber(SearchCriteria searchCriteria) throws ServiceException;
	List<News> search(SearchCriteria searchCriteria, int offset, int number) throws ServiceException;
	long getNewsPosition(SearchCriteria searchCriteria, long newsId) throws ServiceException;
	List<News> getNewsTripplet(SearchCriteria criteria, long newsId) throws ServiceException;
	List<AuthorDTO> getAuthors() throws ServiceException;
	List<TagDTO> getTags() throws ServiceException;
	long addComment(CommentDTO comment) throws ServiceException;
	void deleteComment(long commentId) throws ServiceException;
}

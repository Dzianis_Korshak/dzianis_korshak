package com.epam.newsmanagement.dao.impl;

import static com.epam.newsmanagement.dao.util.DatabaseUtil.closeResources;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.entity.dto.CommentDTO;
import com.epam.newsmanagement.exception.InsertPersistenceException;
import com.epam.newsmanagement.exception.PersistenceException;

@Repository("commentDAO")
public class CommentDAOImpl implements CommentDAO {
	
	private static final String SQL_CREATE_COMMENT = "INSERT INTO COMMENTS(comment_id, news_id, comment_text, creation_date)"
				+ " VALUES(comments_sequence.nextval, ?, ?, ?)";

	private static final String SQL_SELECT_COMMENT 
				= "SELECT comment_id, news_id, comment_text, creation_date FROM comments WHERE comment_id = ?";
	
	private static final String SQL_UPDATE_COMMENT 
				= "UPDATE comments SET news_id = ?, comment_text = ?, creation_date = ? where comment_id = ?";
	
	private static final String SQL_DELETE_COMMENT = "DELETE FROM comments WHERE comment_id = ?";
	
	private static final String SQL_DELETE_COMMENTS_BY_NEWS_ID
				= "DELETE FROM COMMENTS WHERE NEWS_ID = ?";
	
	private static final String SQL_SELECT_COMMENTS_ON_NEWS_ID
				= "SELECT comment_id, news_id, comment_text, creation_date"
				+ " from comments WHERE news_id = ?";
	
	@Autowired
	private DataSource dataSource;
	
	public long create(CommentDTO entity) throws PersistenceException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try{
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(SQL_CREATE_COMMENT, new String[]{"comment_id"});
			preparedStatement.setLong(1, entity.getNewsId());
			preparedStatement.setString(2, entity.getCommentText());
			preparedStatement.setTimestamp(3, new java.sql.Timestamp(entity.getCreationDate().getTime()));
			preparedStatement.executeUpdate();
			resultSet = preparedStatement.getGeneratedKeys();
			if(resultSet.next()){
				return resultSet.getLong(1);
			} else {
				throw new InsertPersistenceException();
			}
		} catch(SQLException ex){
			throw new PersistenceException(ex);
		} finally {
			closeResources(connection, preparedStatement);
		}
	}

	public CommentDTO fetch(long id) throws PersistenceException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try{
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(SQL_SELECT_COMMENT);
			preparedStatement.setLong(1, id);
			resultSet = preparedStatement.executeQuery();
			CommentDTO comment = null;
			if(resultSet.next()){
				comment = getCommentFromResultSet(resultSet);
			}
			if(comment == null){
				throw new PersistenceException("No such author fetched with id = " + id);
			}
			return comment;
		} catch(SQLException ex){
			throw new PersistenceException(ex);
		} finally {
			closeResources(connection, preparedStatement, resultSet);
		}
	}

	public void update(CommentDTO entity) throws PersistenceException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try{
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(SQL_UPDATE_COMMENT);
			preparedStatement.setLong(1, entity.getNewsId());
			preparedStatement.setString(2, entity.getCommentText());
			preparedStatement.setTimestamp(3, new java.sql.Timestamp(entity.getCreationDate().getTime()));
			preparedStatement.setLong(4, entity.getId());
			preparedStatement.executeUpdate();
		} catch(SQLException ex){
			throw new PersistenceException(ex);
		} finally {
			closeResources(connection, preparedStatement);
		}
	}

	public void delete(long id) throws PersistenceException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try{
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(SQL_DELETE_COMMENT);
			preparedStatement.setLong(1, id);
			preparedStatement.executeUpdate();
		} catch(SQLException ex){
			throw new PersistenceException(ex);
		} finally {
			closeResources(connection, preparedStatement);
		}
	}
	
	public void deleteByNewsId(long id) throws PersistenceException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try{
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(SQL_DELETE_COMMENTS_BY_NEWS_ID);
			preparedStatement.setLong(1, id);
			preparedStatement.executeUpdate();
		} catch(SQLException ex){
			throw new PersistenceException(ex);
		} finally {
			closeResources(connection, preparedStatement);
		}
	}
	
	private CommentDTO getCommentFromResultSet(ResultSet resultSet) throws SQLException{
		CommentDTO comment = null;
		comment = new CommentDTO();
		comment.setId(resultSet.getLong(1));
		comment.setNewsId(resultSet.getLong(2));
		comment.setCommentText(resultSet.getString(3));
		comment.setCreationDate(resultSet.getTimestamp(4));
		return comment;
	}

	public List<CommentDTO> fetchByNewsId(long newsId) throws PersistenceException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<CommentDTO> comments = new ArrayList<CommentDTO>();
		try{
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(SQL_SELECT_COMMENTS_ON_NEWS_ID);
			preparedStatement.setLong(1, newsId);
			resultSet = preparedStatement.executeQuery();
			while(resultSet.next()){
				comments.add(getCommentFromResultSet(resultSet));
			}
			return comments;
		} catch(SQLException ex){
			throw new PersistenceException(ex);
		} finally {
			closeResources(connection, preparedStatement, resultSet);
		}
	}
	
}

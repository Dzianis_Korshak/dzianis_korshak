package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.exception.PersistenceException;

public interface CommonDAO<T extends Object> {
	
	public long create(T entity) throws PersistenceException;
	
	public T fetch(long id) throws PersistenceException;
	
	public void update(T entity) throws PersistenceException;
	
	public void delete(long id) throws PersistenceException;
}

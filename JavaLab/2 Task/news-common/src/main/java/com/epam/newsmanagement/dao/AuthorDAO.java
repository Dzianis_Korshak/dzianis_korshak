package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.entity.dto.AuthorDTO;
import com.epam.newsmanagement.exception.PersistenceException;

public interface AuthorDAO extends CommonDAO<AuthorDTO> {
	AuthorDTO fetchByNewsId(long newsId) throws PersistenceException;
	List<AuthorDTO> fetchAll() throws PersistenceException;
}

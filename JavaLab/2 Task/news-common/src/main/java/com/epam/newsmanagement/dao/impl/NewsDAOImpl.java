package com.epam.newsmanagement.dao.impl;

import static com.epam.newsmanagement.dao.util.DatabaseUtil.closeResources;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.entity.dto.NewsDTO;
import com.epam.newsmanagement.exception.InsertPersistenceException;
import com.epam.newsmanagement.exception.PersistenceException;
import com.epam.newsmanagement.util.SearchCriteria;

@Repository("newsDAO")
public class NewsDAOImpl implements NewsDAO {

	private static final String SQL_CREATE_NEWS 
				= "INSERT INTO NEWS(news_id, title, short_text, full_text, creation_date, modification_date)"
				+ " VALUES(news_sequence.nextval, ?, ?, ?, ?, ?)";
	
	private static final String SQL_SELECT_NEWS 
				= "SELECT news_id, title, short_text, full_text, creation_date, modification_date FROM news WHERE news_id = ?";
	
	private static final String SQL_UPDATE_NEWS 
				= "UPDATE news SET title = ?, short_text = ?, full_text = ?, creation_date = ?, modification_date = ? where news_id = ?";
	
	private static final String SQL_DELETE_NEWS 
				= "DELETE FROM news WHERE news_id = ?";
	
	private static final String SQL_DELETE_AUTHOR_REFERENCE
				= "DELETE FROM NEWS_AUTHORS WHERE NEWS_ID = ?";
	
	private static final String SQL_DELETE_TAGS_REFERENCES
				= "DELETE FROM NEWS_TAGS WHERE NEWS_ID = ?";
	
	private static final String SQL_DELETE_NEWS_COMMENTS
				= "DELETE FROM COMMENTS WHERE NEWS_ID = ?";
	
	private static final String SQL_SELECT_ALL_NEWS
				= "select n.news_id, n.TITLE, n.SHORT_TEXT, n.FULL_TEXT, n.CREATION_DATE, n.MODIFICATION_DATE "
				+ "from news n left join comments c on n.news_id = c.news_id "
				+ "group by n.news_id, n.TITLE, n.SHORT_TEXT, n.FULL_TEXT, n.CREATION_DATE, n.MODIFICATION_DATE, c.news_id "
				+ "order by COUNT(c.comment_id) desc";
	
	private static final String SQL_COUNT_NEWS
				= "SELECT COUNT(news.news_id) from news";
	
	private static final String SQL_ATTACH_AUTHOR
				= "INSERT INTO NEWS_AUTHORS(news_id, author_id) VALUES(?,?)";
	
	private static final String SQL_ATTACH_TAGS
				= "INSERT INTO NEWS_TAGS(news_id, tag_id) VALUES(?,?)";
		
	private static final String SQL_SELECT_NEWS_FROM
				= "select news_id, title, short_text, full_text, creation_date, modification_date from";
	
	private static final String SQL_SELECT_NEWS_WHERE
				= "where rownumber >= ? and rownumber < ?";
	
	private static final String SQL_SELECT_ROWNUMBER
				= "select rownumber from";
	
	private static final String SQL_SELECT_ROWNUMBER_ENDING
				= "where news_id = ?";
	
	private static final String SQL_SEARCH_ON_CRITERIA_BEGINNING
				= " ( select news_id, title, short_text, full_text, news.creation_date, modification_date "
				+ ", row_number() over (order by count(distinct comment_id) desc) as rownumber"
				+ " from news inner join news_authors using(news_id)"
				+ " inner join news_tags using(news_id)"
				+ " left join comments using(news_id) ";
	
	private static final String SQL_SEARCH_ON_CRITERIA_ENDING
				= " group by news_id, title, short_text, full_text, news.creation_date, modification_date) ";
	
	
	@Autowired
	private DataSource dataSource;
	
	private int tagGaps = 0;
	
	public long create(NewsDTO entity) throws PersistenceException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try{
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(SQL_CREATE_NEWS, new String[]{"news_id"});
			preparedStatement.setString(1, entity.getTitle());
			preparedStatement.setString(2, entity.getShortText());
			preparedStatement.setString(3, entity.getFullText());
			preparedStatement.setTimestamp(4,  new java.sql.Timestamp(entity.getCreationDate().getTime()));
			preparedStatement.setDate(5, new java.sql.Date(entity.getModificationDate().getTime()));
			preparedStatement.executeUpdate();
			resultSet = preparedStatement.getGeneratedKeys();
			if(resultSet.next()){
				return resultSet.getLong(1);
			} else {
				throw new InsertPersistenceException();
			}
		} catch(SQLException ex){
			throw new PersistenceException(ex);
		} finally {
			closeResources(connection, preparedStatement);
		}
	}

	public NewsDTO fetch(long id) throws PersistenceException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try{
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(SQL_SELECT_NEWS);
			preparedStatement.setLong(1, id);
			resultSet = preparedStatement.executeQuery();
			if(resultSet.next()){
				return getNewsFromResultSet(resultSet);
			}
			throw new PersistenceException("No such author fetched with id = " + id);
		} catch(SQLException ex){
			throw new PersistenceException(ex);
		} finally {
			closeResources(connection, preparedStatement, resultSet);
		}
	}

	public void update(NewsDTO entity) throws PersistenceException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try{
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(SQL_UPDATE_NEWS);
			preparedStatement.setString(1, entity.getTitle());
			preparedStatement.setString(2, entity.getShortText());
			preparedStatement.setString(3, entity.getFullText());
			preparedStatement.setTimestamp(4,  new java.sql.Timestamp(entity.getCreationDate().getTime()));
			preparedStatement.setDate(5, new java.sql.Date(entity.getModificationDate().getTime()));
			preparedStatement.setLong(6, entity.getId());
			preparedStatement.executeUpdate();
		} catch(SQLException ex){
			throw new PersistenceException(ex);
		} finally {
			closeResources(connection, preparedStatement);
		}		
	}

	public void delete(long id) throws PersistenceException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try{
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(SQL_DELETE_NEWS);
			preparedStatement.setLong(1, id);
			preparedStatement.executeUpdate();
		} catch(SQLException ex){
			throw new PersistenceException(ex);
		} finally {
			closeResources(connection, preparedStatement);
		}
	}
	
	public void deleteAuthorReference(long newsId) throws PersistenceException{
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try{
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(SQL_DELETE_AUTHOR_REFERENCE);
			preparedStatement.setLong(1, newsId);
			preparedStatement.executeUpdate();
		} catch(SQLException ex){
			throw new PersistenceException(ex);
		} finally {
			closeResources(connection, preparedStatement);
		}
	}
	
	public void deleteTagsReferences(long newsId) throws PersistenceException{
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try{
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(SQL_DELETE_TAGS_REFERENCES);
			preparedStatement.setLong(1, newsId);
			preparedStatement.executeUpdate();
		} catch(SQLException ex){
			throw new PersistenceException(ex);
		} finally {
			closeResources(connection, preparedStatement);
		}
	}
	
	public void deleteNewsComments(long newsId) throws PersistenceException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try{
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(SQL_DELETE_NEWS_COMMENTS);
			preparedStatement.setLong(1, newsId);
			preparedStatement.executeUpdate();
		} catch(SQLException ex){
			throw new PersistenceException(ex);
		} finally {
			closeResources(connection, preparedStatement);
		}
	}
	
	public List<NewsDTO> fetchAllNews() throws PersistenceException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<NewsDTO> list = new ArrayList<NewsDTO>();
		try{
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(SQL_SELECT_ALL_NEWS);
			resultSet = preparedStatement.executeQuery();
			while(resultSet.next()){
				list.add(getNewsFromResultSet(resultSet));
			}
		} catch(SQLException ex){
			throw new PersistenceException(ex);
		} finally {
			closeResources(connection, preparedStatement, resultSet);
		}
		return list;
	}
	
	public long getNewsCount() throws PersistenceException{
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try{
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(SQL_COUNT_NEWS);
			resultSet = preparedStatement.executeQuery();
			if(resultSet.next()){
				return resultSet.getLong(1);
			}
			throw new PersistenceException();
		} catch(SQLException ex){
			throw new PersistenceException(ex);
		} finally {
			closeResources(connection, preparedStatement, resultSet);
		}
	}
	
	private NewsDTO getNewsFromResultSet(ResultSet resultSet) throws SQLException{
		NewsDTO news = null;
		news = new NewsDTO();
		news.setId(resultSet.getLong(1));
		news.setTitle(resultSet.getString(2));
		news.setShortText(resultSet.getString(3));
		news.setFullText(resultSet.getString(4));
		news.setCreationDate(resultSet.getTimestamp(5));
		news.setModificationDate(resultSet.getDate(6));
		return news;
	}

	public void attachAuthor(long newsId, long authorId) throws PersistenceException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try{
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(SQL_ATTACH_AUTHOR);
			preparedStatement.setLong(1, newsId);
			preparedStatement.setLong(2, authorId);
			preparedStatement.executeUpdate();
		} catch(SQLException ex){
			throw new PersistenceException(ex);
		} finally {
			closeResources(connection, preparedStatement);
		}		
	}

	public void attachTags(long newsId, List<Long> tagIdList) throws PersistenceException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try{
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(SQL_ATTACH_TAGS);
			for(int i = 0; i < tagIdList.size(); i++){
				preparedStatement.setLong(1, newsId);
				preparedStatement.setLong(2, tagIdList.get(i));
				preparedStatement.addBatch();
			}
			preparedStatement.executeBatch();
		} catch(SQLException ex){
			throw new PersistenceException(ex);
		} finally {
			closeResources(connection, preparedStatement);
		}			
	}

	public List<NewsDTO> search(SearchCriteria criteria, long offset, long number) throws PersistenceException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<NewsDTO> list = new ArrayList<NewsDTO>();
		try{
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(SQL_SELECT_NEWS_FROM
					+ generateCriteriaStatement(criteria) + SQL_SELECT_NEWS_WHERE);
			int settedParameters = setSearchParameters(preparedStatement, criteria);
			preparedStatement.setLong(settedParameters+1, offset);
			preparedStatement.setLong(settedParameters+2, offset+number);
			resultSet = preparedStatement.executeQuery();
			while(resultSet.next()){
				list.add(getNewsFromResultSet(resultSet));
			}
		} catch(SQLException ex){
			throw new PersistenceException(ex);
		} finally {
			closeResources(connection, preparedStatement, resultSet);
		}
		return list;
	}

	public long getNewsPosition(SearchCriteria searchCriteria, long newsId) throws PersistenceException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try{
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(SQL_SELECT_ROWNUMBER
					+ generateCriteriaStatement(searchCriteria) + SQL_SELECT_ROWNUMBER_ENDING);
			int settedParameters = setSearchParameters(preparedStatement, searchCriteria);
			preparedStatement.setLong(settedParameters+1, newsId);
			resultSet = preparedStatement.executeQuery();
			if(resultSet.next()){
				return resultSet.getLong(1);
			}
			throw new PersistenceException();
		} catch(SQLException ex){
			throw new PersistenceException(ex);
		} finally {
			closeResources(connection, preparedStatement, resultSet);
		}
	}
	
	private int setSearchParameters(PreparedStatement preparedStatement, SearchCriteria searchCriteria) throws SQLException{
		int current = 0;
		if(searchCriteria == null){
			return 0;
		}
		long authorId = searchCriteria.getAuthorId();
		if(authorId != 0){
			preparedStatement.setLong(1, authorId);
			current++;
		}
		for(int i = 0; i < tagGaps; i++){
			current++;
			if(i < searchCriteria.getTagsCount()){
				preparedStatement.setLong(current, searchCriteria.getTagId(i));
			} else {
				preparedStatement.setNull(current, java.sql.Types.BIGINT);
			}	
		}
		return current;
	}
	
	private String generateCriteriaStatement(SearchCriteria criteria){
		tagGaps = 0;
		StringBuilder builder = new StringBuilder(SQL_SEARCH_ON_CRITERIA_BEGINNING);
		if(criteria == null){
			builder.append(SQL_SEARCH_ON_CRITERIA_ENDING);
			return builder.toString();
		}
		long authorId = criteria.getAuthorId();
		List<Long> tags = criteria.getTagIdList();
		if(authorId != 0){
			builder.append("where author_id = ? ");
			if(!tags.isEmpty()){
				builder.append("and"+generateTagStatement(tags.size()));
			}
		} else if(!tags.isEmpty()){
			builder.append("where" + generateTagStatement(tags.size()));
		}
		builder.append(SQL_SEARCH_ON_CRITERIA_ENDING);
		return builder.toString();
	}
	
	private String generateTagStatement(int tagNumber){
		if(tagNumber <= 5){
			tagGaps = 5;
		}else if(tagNumber <= 10){
			tagGaps = 10;
		}else if(tagNumber <= 30){
			tagGaps = 30;
		}else if(tagNumber <= 60){
			tagGaps = 60;
		}else{
			tagGaps = tagNumber;
		}
		StringBuilder sqlStatement = new StringBuilder(" tag_id in (");
		for(int i = 0; i < tagGaps; i++){
			sqlStatement.append("?,");
		}
		sqlStatement.setCharAt(sqlStatement.length()-1, ')');
		return sqlStatement.toString();
	}
}

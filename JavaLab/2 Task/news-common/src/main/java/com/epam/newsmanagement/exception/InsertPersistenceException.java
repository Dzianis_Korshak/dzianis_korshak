package com.epam.newsmanagement.exception;

public class InsertPersistenceException extends PersistenceException {

	private static final long serialVersionUID = -1819937112819692209L;

	public InsertPersistenceException() {
		super();
	}

	public InsertPersistenceException(String message, Throwable cause) {
		super(message, cause);
	}

	public InsertPersistenceException(String message) {
		super(message);
	}

	public InsertPersistenceException(Throwable cause) {
		super(cause);
	}

}

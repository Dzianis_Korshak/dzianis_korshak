package com.epam.newsmanagement.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.entity.dto.CommentDTO;
import com.epam.newsmanagement.exception.PersistenceException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.CommentService;

@Service("commentService")
public class CommentServiceImpl implements CommentService {
	
	private static final Logger LOG = Logger.getLogger(CommentServiceImpl.class);
	
	@Autowired
	private CommentDAO commentDAO;
	
	public long create(CommentDTO entity) throws ServiceException {
		try{
			return commentDAO.create(entity);
		} catch (PersistenceException ex){
			LOG.error(ex.getMessage(),ex);
			throw new ServiceException(ex);
		}
	}

	public CommentDTO fetch(long id) throws ServiceException {
		try{
			return commentDAO.fetch(id);
		} catch(PersistenceException ex){
			LOG.error(ex.getMessage(),ex);
			throw new ServiceException(ex);
		}
	}

	public void update(CommentDTO entity) throws ServiceException {
		try{
			commentDAO.update(entity);
		} catch(PersistenceException ex){
			LOG.error(ex.getMessage(),ex);
			throw new ServiceException(ex);
		}
	}

	public void delete(long id) throws ServiceException {
		try{
			commentDAO.delete(id);
		} catch(PersistenceException ex){
			LOG.error(ex.getMessage(),ex);
			throw new ServiceException(ex);
		}
	}

	public void deleteByNewsId(long newsId) throws ServiceException {
		try{
			commentDAO.deleteByNewsId(newsId);
		} catch(PersistenceException ex){
			LOG.error(ex.getMessage(),ex);
			throw new ServiceException(ex);
		}
	}

	public List<CommentDTO> fetchByNewsId(long newsId) throws ServiceException {
		try{
			return commentDAO.fetchByNewsId(newsId);
		} catch(PersistenceException ex){
			LOG.error(ex.getMessage(),ex);
			throw new ServiceException(ex);
		}
	}

}

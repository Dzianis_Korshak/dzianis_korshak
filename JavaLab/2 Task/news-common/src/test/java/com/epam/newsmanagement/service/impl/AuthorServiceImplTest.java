package com.epam.newsmanagement.service.impl;

import static org.mockito.Mockito.*;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.entity.dto.AuthorDTO;
import com.epam.newsmanagement.exception.PersistenceException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.impl.AuthorServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class AuthorServiceImplTest {
		
	private AuthorDTO author = new AuthorDTO();
	
	@Mock
	private AuthorDAO authorDAO;
	
	@InjectMocks
	private AuthorServiceImpl authorService;
	
	@Test
	public void testCreate() throws ServiceException, PersistenceException{
		long actualId, expectedId = 123;
		when(authorDAO.create(author)).thenReturn(expectedId);
		actualId = authorService.create(author);
		assertEquals(expectedId, actualId);
	}
	
	@Test(expected=ServiceException.class)
	public void testCreateWithServiceException() throws PersistenceException, ServiceException{
		doThrow(PersistenceException.class).when(authorDAO).create(any(AuthorDTO.class));
		authorService.create(author);
	}
	
	@Test
	public void testFetch() throws ServiceException, PersistenceException{
		when(authorDAO.fetch(anyLong())).thenReturn(author);
		AuthorDTO actualAuthor = authorService.fetch(12);
		assertEquals(author, actualAuthor);
	}
	
	@Test(expected=ServiceException.class)
	public void testFetchWithServiceException() throws ServiceException, PersistenceException{
		doThrow(PersistenceException.class).when(authorDAO).fetch(anyLong());
		authorService.fetch(22);
	}
	
	@Test
	public void testUpdate() throws ServiceException, PersistenceException {
		authorService.update(author);
		verify(authorDAO).update(author);
	}
	
	@Test(expected = ServiceException.class)
	public void testUpdateWithServiceException() throws PersistenceException, ServiceException {
		doThrow(PersistenceException.class).when(authorDAO).update(any(AuthorDTO.class));
		authorService.update(author);
	}
	
	@Test
	public void testDelete() throws ServiceException, PersistenceException {
		long authorId = 1L;
		authorService.delete(authorId);
		verify(authorDAO).delete(authorId);
	}
	
	@Test(expected = ServiceException.class)
	public void testDeleteWithServiceException() throws PersistenceException, ServiceException {
		doThrow(PersistenceException.class).when(authorDAO).delete(anyLong());
		authorService.delete(1);
	}
	
	@Test
	public void testFetchByNewsId() throws ServiceException, PersistenceException{
		when(authorDAO.fetchByNewsId(anyLong())).thenReturn(author);
		AuthorDTO actualAuthor = authorService.fetchByNewsId(12);
		assertEquals(author, actualAuthor);
	}
	
}

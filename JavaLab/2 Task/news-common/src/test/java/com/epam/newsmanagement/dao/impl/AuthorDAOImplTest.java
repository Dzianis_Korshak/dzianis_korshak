package com.epam.newsmanagement.dao.impl;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.SQLException;

import org.dbunit.DatabaseUnitException;
import org.dbunit.assertion.DbUnitAssert;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.entity.dto.AuthorDTO;
import com.epam.newsmanagement.exception.PersistenceException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DbUnitConfiguration;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/test-config.xml"})
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
@DbUnitConfiguration(databaseConnection={"testDatabaseConnection"})
@DatabaseSetup(value="/testdata/initialData.xml")
public class AuthorDAOImplTest {
	
	@Autowired
	private AuthorDAO authorDAO;
	
	@Autowired
	private IDatabaseConnection databaseConnection;
	
	@Test
	public void testCreate() throws PersistenceException, IOException, SQLException, DatabaseUnitException{
		AuthorDTO author = new AuthorDTO();
		author.setName("Petya Poroh");
		authorDAO.create(author);
		
		FlatXmlDataSetBuilder xmlBuilder = new FlatXmlDataSetBuilder();
		InputStream inputStream = ClassLoader.getSystemResourceAsStream("testdata/afterInsertData.xml");
		IDataSet expectedDataset = xmlBuilder.build(new InputStreamReader(inputStream, "UTF-8"));	
		IDataSet actualDataset = databaseConnection.createDataSet();

		(new DbUnitAssert()).assertEqualsIgnoreCols(expectedDataset, actualDataset
				,"AUTHORS", new String[]{"AUTHOR_ID", "EXPIRED"});
	}
	
	@Test
	public void testFetch() throws PersistenceException{
		AuthorDTO expectedAuthor, actualAuthor;
		expectedAuthor = new AuthorDTO(1001,"Denis Shmenis");
		actualAuthor = authorDAO.fetch(1001);
		Assert.assertEquals(expectedAuthor, actualAuthor);
	}
	
	@ExpectedDatabase(table="AUTHORS", value="/testdata/afterUpdateData.xml", assertionMode=DatabaseAssertionMode.NON_STRICT_UNORDERED)
	@Test
	public void testUpdate() throws PersistenceException{
		authorDAO.update(new AuthorDTO(1001,"Denis Hrenis"));
	}
	
	@ExpectedDatabase(table="AUTHORS", value="/testdata/afterDeleteData.xml", assertionMode=DatabaseAssertionMode.NON_STRICT_UNORDERED)
	@Test
	public void testDelete() throws PersistenceException{
		authorDAO.delete(1002);
	}
	
	@Test
	public void testFetchOnNewsId() throws PersistenceException{
		AuthorDTO expectedAuthor, actualAuthor;
		expectedAuthor = new AuthorDTO(1001,"Denis Shmenis");
		actualAuthor = authorDAO.fetchByNewsId(1002);
		Assert.assertEquals(expectedAuthor, actualAuthor);
	}
}

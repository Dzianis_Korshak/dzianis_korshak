package com.epam.newsmanagement.dao.impl;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.dbunit.DatabaseUnitException;
import org.dbunit.assertion.DbUnitAssert;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.epam.newsmanagement.dao.impl.TagDAOImpl;
import com.epam.newsmanagement.entity.dto.TagDTO;
import com.epam.newsmanagement.exception.PersistenceException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DbUnitConfiguration;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/test-config.xml"})
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
@DbUnitConfiguration(databaseConnection={"testDatabaseConnection"})
@DatabaseSetup(value="/testdata/initialData.xml")
public class TagDAOImplTest {

	@Autowired
	private TagDAOImpl tagDAO;
	
	@Autowired
	private IDatabaseConnection databaseConnection;
	
	@Test
	public void testCreate() throws PersistenceException, IOException, SQLException, DatabaseUnitException{
		TagDTO tag = new TagDTO();
		tag.setTagName("Brand new");
		tagDAO.create(tag);
		
		FlatXmlDataSetBuilder xmlBuilder = new FlatXmlDataSetBuilder();
		InputStream inputStream = ClassLoader.getSystemResourceAsStream("testdata/afterInsertData.xml");
		IDataSet expectedDataset = xmlBuilder.build(new InputStreamReader(inputStream, "UTF-8"));	
		IDataSet actualDataset = databaseConnection.createDataSet();

		(new DbUnitAssert()).assertEqualsIgnoreCols(expectedDataset, actualDataset
				,"TAGS", new String[]{"TAG_ID"});
	}
	
	@Test
	public void testFetch() throws PersistenceException{
		TagDTO expectedTag, actualTag;
		expectedTag = new TagDTO(1001,"Coca Cola");
		actualTag = tagDAO.fetch(1001);
		Assert.assertEquals(expectedTag, actualTag);
	}
	
	@ExpectedDatabase(table="TAGS", value="/testdata/afterUpdateData.xml", assertionMode=DatabaseAssertionMode.NON_STRICT_UNORDERED)
	@Test
	public void testUpdate() throws PersistenceException{
		tagDAO.update(new TagDTO(1001,"Bela Cola"));
	}
	
	@ExpectedDatabase(table="TAGS", value="/testdata/afterDeleteData.xml", assertionMode=DatabaseAssertionMode.NON_STRICT_UNORDERED)
	@Test
	public void testDelete() throws PersistenceException{
		tagDAO.delete(1002);
	}
	
	@SuppressWarnings("serial")
	@Test
	public void testFetchByNewsId() throws PersistenceException{
		List<TagDTO> expectedTags = new ArrayList<TagDTO>(){{
			add(new TagDTO(1001,"Coca Cola"));
			add(new TagDTO(1003,"Hot"));
		}}, actualTags = null;
		actualTags = tagDAO.fetchByNewsId(1002);
		Assert.assertEquals(expectedTags, actualTags);
	}
	
}

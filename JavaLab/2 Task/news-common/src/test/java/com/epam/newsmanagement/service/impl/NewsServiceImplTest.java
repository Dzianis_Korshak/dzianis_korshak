package com.epam.newsmanagement.service.impl;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.entity.dto.NewsDTO;
import com.epam.newsmanagement.exception.PersistenceException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.impl.NewsServiceImpl;
import com.epam.newsmanagement.util.SearchCriteria;


@RunWith(MockitoJUnitRunner.class)
public class NewsServiceImplTest {
	
	private NewsDTO news = new NewsDTO();
	private List<NewsDTO> newsList = new ArrayList<NewsDTO>();
	
	@Mock
	private NewsDAO newsDAO;
	
	@InjectMocks
	private NewsServiceImpl newsService;
	
	@Test
	public void testCreate() throws ServiceException, PersistenceException{
		long actualId, expectedId = 123;
		when(newsDAO.create(news)).thenReturn(expectedId);
		actualId = newsService.create(news);
		assertEquals(expectedId, actualId);
	}
	
	@Test(expected=ServiceException.class)
	public void testCreateWithServiceException() throws PersistenceException, ServiceException{
		doThrow(PersistenceException.class).when(newsDAO).create(any(NewsDTO.class));
		newsService.create(news);
	}
	
	@Test
	public void testFetch() throws ServiceException, PersistenceException{
		when(newsDAO.fetch(anyLong())).thenReturn(news);
		NewsDTO actualNews = newsService.fetch(12);
		assertEquals(news, actualNews);
	}
	
	@Test(expected=ServiceException.class)
	public void testFetchWithServiceException() throws ServiceException, PersistenceException{
		doThrow(PersistenceException.class).when(newsDAO).fetch(anyLong());
		newsService.fetch(22);
	}
	
	@Test
	public void testUpdate() throws ServiceException, PersistenceException {
		newsService.update(news);
		verify(newsDAO).update(news);
	}
	
	@Test(expected = ServiceException.class)
	public void testUpdateWithServiceException() throws PersistenceException, ServiceException {
		doThrow(PersistenceException.class).when(newsDAO).update(any(NewsDTO.class));
		newsService.update(news);
	}
	
	@Test
	public void testDelete() throws ServiceException, PersistenceException {
		long newsId = 1L;
		newsService.delete(newsId);
		verify(newsDAO).delete(newsId);
	}
	
	@Test(expected = ServiceException.class)
	public void testDeleteWithServiceException() throws PersistenceException, ServiceException {
		doThrow(PersistenceException.class).when(newsDAO).delete(anyLong());
		newsService.delete(anyLong());
	}
	
	@Test
	public void testDeleteAuthorReference() throws PersistenceException, ServiceException {
		long newsId = 1L;
		newsService.deleteAuthorReference(newsId);
		verify(newsDAO).deleteAuthorReference(newsId);
	}
	
	@Test(expected = ServiceException.class)
	public void testDeleteAuthorReferenceWithServiceException() throws PersistenceException, ServiceException {
		doThrow(PersistenceException.class).when(newsDAO).deleteAuthorReference(anyLong());
		newsService.deleteAuthorReference(anyLong());
	}
	
	@Test
	public void testDeleteTagsReferences() throws PersistenceException, ServiceException {
		long newsId = 1L;
		newsService.deleteTagsReferences(newsId);
		verify(newsDAO).deleteTagsReferences(newsId);
	}
	
	@Test(expected = ServiceException.class)
	public void testDeleteTagsReferencesWithServiceException() throws PersistenceException, ServiceException {
		doThrow(PersistenceException.class).when(newsDAO).deleteTagsReferences(anyLong());
		newsService.deleteTagsReferences(anyLong());
	}
	
	@Test
	public void testDeleteNewsComments() throws PersistenceException, ServiceException {
		long newsId = 1L;
		newsService.deleteNewsComments(newsId);
		verify(newsDAO).deleteNewsComments(newsId);
	}
	
	@Test(expected = ServiceException.class)
	public void testDeleteNewsCommentsWithServiceException() throws PersistenceException, ServiceException {
		doThrow(PersistenceException.class).when(newsDAO).deleteNewsComments(anyLong());
		newsService.deleteNewsComments(anyLong());
	}
	
	@Test
	public void testFetchAllNews() throws PersistenceException, ServiceException{
		when(newsDAO.fetchAllNews()).thenReturn(newsList);
		assertEquals(newsList, newsService.fetchAllNews());
	}
	
	@Test(expected=ServiceException.class)
	public void testFetchAllNewsWithServiceException() throws PersistenceException, ServiceException{
		doThrow(PersistenceException.class).when(newsDAO).fetchAllNews();
		newsService.fetchAllNews();
	}
	
	@Test
	public void testGetNewsCount() throws PersistenceException, ServiceException{
		long count = 123;
		when(newsDAO.getNewsCount()).thenReturn(count);
		assertEquals(count, newsService.getNewsNumber());
	}
	
	@Test(expected=ServiceException.class)
	public void testGetNewsCountWithServiceException() throws PersistenceException, ServiceException{
		doThrow(PersistenceException.class).when(newsDAO).getNewsCount();
		newsService.getNewsNumber();
	}
	
	@Test
	public void testAttachAuthor() throws PersistenceException, ServiceException{
		long newsId = 1, authorId = 1;
		newsService.attachAuthor(newsId, authorId);
		verify(newsDAO).attachAuthor(newsId, authorId);
	}
	
	@Test(expected=ServiceException.class)
	public void testAttachAuthorWithServiceException() throws PersistenceException, ServiceException{
		doThrow(PersistenceException.class).when(newsDAO).attachAuthor(anyLong(), anyLong());
		newsService.attachAuthor(anyLong(), anyLong());
	}
	
	@Test
	public void testAttachTags() throws PersistenceException, ServiceException{
		long newsId = 1;
		List<Long> tagIdList = Arrays.asList(1L,2L,3L);
		newsService.attachTags(newsId, tagIdList);
		verify(newsDAO).attachTags(newsId, tagIdList);
	}
	
	@Test(expected=ServiceException.class)
	public void testAttachTagsWithServiceException() throws PersistenceException, ServiceException{
		List<Long> tagIdList = Arrays.asList(1L,2L,3L);
		doThrow(PersistenceException.class).when(newsDAO).attachTags(anyLong(), eq(tagIdList));
		newsService.attachTags(1L, tagIdList);
	}
	
	@Test
	public void testSearch() throws PersistenceException, ServiceException{
		List<NewsDTO> expectedList = new ArrayList<NewsDTO>();
		when(newsDAO.search(any(SearchCriteria.class),anyInt(),anyInt())).thenReturn(expectedList);
		List<NewsDTO> actualList = newsService.search(any(SearchCriteria.class),anyInt(),anyInt());
		assertEquals(expectedList, actualList);
	}
}

package com.epam.newsmanagement.service.impl;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.entity.dto.TagDTO;
import com.epam.newsmanagement.exception.PersistenceException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.impl.TagServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class TagServiceImplTest {
	
	private TagDTO tag = new TagDTO();
	
	@Mock
	private TagDAO tagDAO;
	
	@InjectMocks
	private TagServiceImpl tagService;
	
	@Test
	public void testCreate() throws ServiceException, PersistenceException{
		long actualId, expectedId = 123;
		when(tagDAO.create(tag)).thenReturn(expectedId);
		actualId = tagService.create(tag);
		assertEquals(expectedId, actualId);
	}
	
	@Test(expected=ServiceException.class)
	public void testCreateWithServiceException() throws PersistenceException, ServiceException{
		doThrow(PersistenceException.class).when(tagDAO).create(any(TagDTO.class));
		tagService.create(tag);
	}
	
	@Test
	public void testFetch() throws ServiceException, PersistenceException{
		when(tagDAO.fetch(anyLong())).thenReturn(tag);
		TagDTO actualTag = tagService.fetch(anyLong());
		assertEquals(tag, actualTag);
	}
	
	@Test(expected=ServiceException.class)
	public void testFetchWithServiceException() throws ServiceException, PersistenceException{
		doThrow(PersistenceException.class).when(tagDAO).fetch(anyLong());
		tagService.fetch(anyLong());
	}
	
	@Test
	public void testUpdate() throws ServiceException, PersistenceException {
		tagService.update(tag);
		verify(tagDAO).update(tag);
	}
	
	@Test(expected = ServiceException.class)
	public void testUpdateWithServiceException() throws PersistenceException, ServiceException {
		doThrow(PersistenceException.class).when(tagDAO).update(any(TagDTO.class));
		tagService.update(tag);
	}
	
	@Test
	public void testDelete() throws ServiceException, PersistenceException {
		long tagId = 1L;
		tagService.delete(tagId);
		verify(tagDAO).delete(tagId);
	}
	
	@Test(expected = ServiceException.class)
	public void testDeleteWithServiceException() throws PersistenceException, ServiceException {
		doThrow(PersistenceException.class).when(tagDAO).delete(anyLong());
		tagService.delete(anyLong());
	}
	
	@Test
	public void testFetchByNewsId() throws ServiceException, PersistenceException{
		List<TagDTO> expectedTags = new ArrayList<TagDTO>(), actualTags = null;
		when(tagDAO.fetchByNewsId(anyLong())).thenReturn(expectedTags);
		actualTags = tagService.fetchByNewsId(anyLong());
		assertEquals(expectedTags, actualTags);
	}
	
}

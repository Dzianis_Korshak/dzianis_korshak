drop table comments;
drop table news_authors;
drop table news_tags;
drop table tags;
drop table authors;
drop table news;
drop table roles;
drop table users;
drop sequence news_sequence;
drop sequence authors_sequence;
drop sequence tags_sequence;
drop sequence comments_sequence;
drop sequence users_sequence;

CREATE TABLE news(
  news_id NUMBER(20),
  title NVARCHAR2(30) not null,
  short_text NVARCHAR2(100) not null,
  full_text NVARCHAR2(2000) not null,
  creation_date TIMESTAMP not null,
  modification_date DATE not null,
  CONSTRAINT news_pk PRIMARY KEY (news_id)
);

CREATE TABLE authors(
  author_id NUMBER(20),
  author_name NVARCHAR2(30) not null,
  expired TIMESTAMP,
  CONSTRAINT authors_pk PRIMARY KEY (author_id)
);

CREATE TABLE news_authors(
  news_id NUMBER(20) not null,
  CONSTRAINT na_news_id_fk FOREIGN KEY (news_id)
    REFERENCES news(news_id),
  author_id NUMBER(20) not null,
  CONSTRAINT na_author_id_fk FOREIGN KEY (author_id)
    REFERENCES authors(author_id)
);

CREATE TABLE tags(
  tag_id NUMBER(20),
  tag_name NVARCHAR2(30) not null,
  CONSTRAINT tags_pk PRIMARY KEY (tag_id)
);

CREATE TABLE news_tags(
  news_id NUMBER(20) not null,
  CONSTRAINT nt_news_id_fk FOREIGN KEY (news_id)
    REFERENCES news(news_id),
  tag_id NUMBER(20) not null,
  CONSTRAINT nt_tag_id_fk FOREIGN KEY (tag_id)
    REFERENCES tags(tag_id)
);

CREATE TABLE comments(
  comment_id NUMBER(20),
  news_id NUMBER(20) not null,
  CONSTRAINT comments_news_news_id_fk FOREIGN KEY(news_id)
    REFERENCES news(news_id),
  comment_text NVARCHAR2(100) not null,
  creation_date TIMESTAMP not null,
  CONSTRAINT comments_pk PRIMARY KEY (comment_id)
);

CREATE TABLE users(
  user_id NUMBER(20),
  user_name NVARCHAR2(50) not null,
  login VARCHAR2(30) not null,
  password varchar2(30) not null,
  CONSTRAINT users_pk PRIMARY KEY(user_id)
);

CREATE TABLE roles(
  user_id NUMBER(20) not null,
  CONSTRAINT roles_users_user_id_fk FOREIGN KEY(user_id)
    REFERENCES users(user_id),
  role_name VARCHAR2(50) not null
);

CREATE SEQUENCE authors_sequence start with 1 increment by 1;
CREATE SEQUENCE comments_sequence start with 1 increment by 1;
CREATE SEQUENCE news_sequence start with 1 increment by 1;
CREATE SEQUENCE tags_sequence start with 1 increment by 1;
CREATE SEQUENCE users_sequence start with 1 increment by 1;
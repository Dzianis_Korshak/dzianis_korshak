package com.epam.newsmanagement.util.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.epam.newsmanagement.entity.dto.CommentDTO;

@Component("commentValidator")
public class CommentValidator implements Validator{

	private static final int COUNT_CHARACTER = 100;
	
	@Override
	public boolean supports(Class<?> paramClass) {
		return CommentDTO.class.equals(paramClass);
	}

	@Override
	public void validate(Object object, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "commentText", "valid.comment.commenttext");
		CommentDTO comment = (CommentDTO) object;
		if(comment.getCommentText().length() > COUNT_CHARACTER){
			errors.rejectValue("commentText", "valid.comment.commenttext.size");
		}
	}
}

package com.epam.newsmanagement.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.epam.newsmanagement.util.ConfigurationManager;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.dto.AuthorDTO;
import com.epam.newsmanagement.entity.dto.TagDTO;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.NewsManager;
import com.epam.newsmanagement.util.SearchCriteria;
import com.epam.newsmanagement.util.SearchCriteriaUtil;

@Controller
@RequestMapping("/news-list")
public class NewsListController {
	
	@Autowired
	private NewsManager newsManager;
	
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView getNewsList(@RequestParam(required = false, defaultValue = "1")Integer page
									, HttpSession session) throws ServiceException{
		ModelAndView modelAndView = new ModelAndView("news-list");
		int newsPerPage = getNewsPerPageNumber(session);
		SearchCriteria searchCriteria = SearchCriteriaUtil.getSearchCriteria(session);

		List<News> newsList = newsManager.search(searchCriteria,(page-1)*newsPerPage+1, newsPerPage);
		long totalNewsNumber = newsManager.getNewsNumber(searchCriteria);
		List<AuthorDTO> authorList = newsManager.getAuthors();
		List<TagDTO> tagList = newsManager.getTags();
		
		modelAndView.addObject("totalNewsNumber", totalNewsNumber);
		modelAndView.addObject("newsList", newsList);
		modelAndView.addObject("authorList", authorList);
		modelAndView.addObject("tagList", tagList);
		modelAndView.addObject("page", page);
		return modelAndView;
	}
	
	@RequestMapping(value = "/filter", method = RequestMethod.POST)
	public String setSearchCriteria(HttpSession session
			, @RequestParam(required = false, defaultValue = "0") Long author
			, @RequestParam(required = false, value = "selectedTags[]") List<Long> selectedTags){
		SearchCriteria searchCriteria = SearchCriteriaUtil.getSearchCriteria(session);
		searchCriteria.setAuthorId(author);
		searchCriteria.setTagIdList(selectedTags);
		return "redirect:/news-list";
	}
		
	private int getNewsPerPageNumber(HttpSession session){
		Object attrValue = session.getAttribute("newsPerPage");
		if(attrValue == null || !(attrValue instanceof Integer)){
			Integer defaultValue = Integer.parseInt(ConfigurationManager.getProperty("config.news-per-page"));
			session.setAttribute("newsPerPage", defaultValue);
			return defaultValue;
		}
		return (Integer)attrValue;
	}

}

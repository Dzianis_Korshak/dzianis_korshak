package com.epam.newsmanagement.util;

import javax.servlet.http.HttpSession;

public class SearchCriteriaUtil {
	private static final String SEARCH_CRITERIA_ATTR = "searchCriteria";
	
	public static SearchCriteria getSearchCriteria(HttpSession session){
		SearchCriteria searchCriteria = (SearchCriteria)session.getAttribute(SEARCH_CRITERIA_ATTR);
		if(searchCriteria == null){
			searchCriteria = new SearchCriteria();
			session.setAttribute(SEARCH_CRITERIA_ATTR, searchCriteria);
		}
		return searchCriteria;
	}
	
}

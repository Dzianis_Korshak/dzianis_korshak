package com.epam.newsmanagement.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.dto.AuthorDTO;
import com.epam.newsmanagement.entity.dto.CommentDTO;
import com.epam.newsmanagement.entity.dto.NewsDTO;
import com.epam.newsmanagement.entity.dto.TagDTO;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.NewsManager;
import com.epam.newsmanagement.util.SearchCriteriaUtil;

@Controller
@RequestMapping(value="/news")
public class NewsController {

	@Autowired
	private NewsManager newsManager;
	
	@Autowired
	@Qualifier("commentValidator")
	private Validator validator;
	
	@RequestMapping(value="{newsId}",method = RequestMethod.GET)
	public ModelAndView viewNews(@PathVariable("newsId") Long newsId
								,HttpSession session) throws ServiceException{
		List<News> newsTripplet = newsManager.getNewsTripplet(SearchCriteriaUtil.getSearchCriteria(session), newsId);
		ModelAndView modelAndView = new ModelAndView("view-news");
		modelAndView.addObject("prev", newsTripplet.get(0));
		modelAndView.addObject("article", newsTripplet.get(1));
		modelAndView.addObject("next", newsTripplet.get(2));
		modelAndView.addObject("comment", new CommentDTO());
		return modelAndView;
	}
	
	@RequestMapping(value="{newsId}/post-comment",method = RequestMethod.POST)
	public String postComment(@PathVariable("newsId") Long newsId
								, @ModelAttribute("comment") @Validated CommentDTO comment
								, HttpSession session, BindingResult bindingResult) throws ServiceException{
		validator.validate(comment, bindingResult);
		if(!bindingResult.hasErrors()){
			comment.setNewsId(newsId);
			comment.setCreationDate(new Date());
			newsManager.addComment(comment);
		}
		return "redirect:/news/{newsId}";
	}
	
	@RequestMapping(value="{newsId}/delete-comment", method = RequestMethod.POST)
	public String deleteComment(@PathVariable("newsId") Long newsId
								, @RequestParam(required=true) Long commentId) throws ServiceException{
		newsManager.deleteComment(commentId);
		return "redirect:/news/{newsId}";
	}
	
	@RequestMapping(value="add", method = RequestMethod.GET)
	public ModelAndView prepareAddNewsView() throws ServiceException{
		List<AuthorDTO> authorList = newsManager.getAuthors();
		List<TagDTO> tagList = newsManager.getTags();
		NewsDTO news = new NewsDTO();
		news.setCreationDate(new Date());
		ModelAndView modelAndView = new ModelAndView("add-news");
		modelAndView.addObject("authorList", authorList);
		modelAndView.addObject("tagList", tagList);
		modelAndView.addObject("news", news);
		return modelAndView;
	}
	
	@RequestMapping(value="add", method = RequestMethod.POST)
	public String addNews(@ModelAttribute("news") NewsDTO news
			, @RequestParam(required = false, value = "selectedAuthor") Long selectedAuthor
			, @RequestParam(required = false, value = "selected-tags[]") List<Long> selectedTags) throws ServiceException{
		System.out.println("PopopOpoPoPOPopoPOp");
		Long newsId = newsManager.saveNews(news, selectedAuthor, selectedTags);
		return "redirect:/news/" + newsId;
	}
	
}

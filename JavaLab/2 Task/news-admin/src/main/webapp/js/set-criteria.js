$(document).ready(function(){
	var optionText = $('.multiselect option[selected]').text();
	
	var setSelectedTagNumber = function(){
		var selectedTags = [];
        $('input[name="selected-tags[]"]:checked').each(function(i){
        	selectedTags[i] = $(this).val();
        });
        $('.multiselect option[selected]').text(optionText + '(' + selectedTags.length + ')');
	};
	
	setSelectedTagNumber();
	
	$('#search').click(function(event) {
		var author = $('select[name="selected-author"]').val();
		var selectedTags = [];
        $('input[name="selected-tags[]"]:checked').each(function(i){
        	selectedTags[i] = $(this).val();
        });
		$.post('news-list/filter',{author:author,'selectedTags[]':selectedTags},function(data){
	        $('body').html(data);
		});
	});
	
	$('#reset').click(function(event) {
		$.post('news-list/filter',function(data){
	        $('body').html(data);
		});
	});
	
	$('input[name="selected-tags[]"]').click(function(event){
		setSelectedTagNumber();
	});
	
	
});
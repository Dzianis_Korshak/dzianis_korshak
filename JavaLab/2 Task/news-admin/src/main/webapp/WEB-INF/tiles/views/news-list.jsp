<%@ taglib prefix="ctg" uri="news-paginator" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript" src="<c:url value='/js/jquery-2.1.1.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/multi-select.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/set-criteria.js'/>"></script>
<link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet"/>

<form>
	<div class="select-wrapper">
		<select name="selected-author">
    		<option disabled selected="selected">Select an option</option>
   			<c:forEach items="${authorList}" var="author">
				<option value="${author.id}" ${author.id == searchCriteria.authorId ? 'selected="selected"' : ''}>${author.name }</option>
			</c:forEach>
   		</select>
	</div>
    <div class="select-wrapper">
        <div class="select-box multiselect">
            <select>
                <option selected disabled>Select an option</option>
            </select>
            <div class="overSelect"></div>
        </div>
        <div class="checkboxes">
        	<label disabled><input type="checkbox"/>Select an option</label>
        	<c:forEach items="${tagList}" var="tag">
				<label><input name="selected-tags[]" type="checkbox" value="${tag.id}"
					${searchCriteria.contains(tag.id) ? 'checked="checked"' : ''}/>${tag.tagName }</label>
			</c:forEach>
        </div>
    </div>
    <input type="button" id="search" value="Filter"/>
    <input type="button" id="reset" value="Reset"/>
</form>

<ctg:news newsList="${newsList}" pageNumber="${page}" totalNewsNumber="${totalNewsNumber}" 
	newsPerPage="3" paginationUrlPrefix="news-list?page="
	viewNewsUrlPrefix="news/">
	<a href="news/{newsId}">View</a>
</ctg:news>
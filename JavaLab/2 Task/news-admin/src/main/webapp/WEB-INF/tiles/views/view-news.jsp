<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<a href="../news-list" id="back-ref">Back</a>
<div class="content">
	<div class="news-title">
		${article.news.title}
	</div>
	<span class="author">(${article.author.name})</span>
	<div class="date">${article.news.modificationDate }</div>
	<div class="news-text">${article.news.fullText }</div>
	<div class="comment-list">
		<c:forEach items="${article.commentList}" var="cmnt">
			<div class="date">${cmnt.creationDate }</div>
			<div class="comment-text">
				${cmnt.commentText }
				<form action="${newsId }/delete-comment" method="POST">
					<input type="hidden" name="commentId" value="${cmnt.id}"/>
					<input id="deleteComment" type="submit"  value="X"/>
				</form>
			</div>
		</c:forEach>
		<form:form class="comment-form" method="POST" action="${newsId}/post-comment" commandName="comment">
			<form:textarea rows="4" cols="60" path="commentText"/>
			<form:errors path="commentText"/>
			<input id="post-comment" type="submit" value="Post comment"/>
		</form:form>
	</div>
</div>
<c:if test="${prev != null}">
	<a href="${prev.news.id}">PREV</a>
</c:if>
<c:if test="${next != null}">
	<a href="${next.news.id}">NEXT</a>
</c:if>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<security:authorize access="hasRole('ROLE_ADMIN')">
	<c:url value="/logout" var="logoutUrl" />
	<tr align="center">
		<td>
			<!--spring:message code="title.hello"/-->Hello, <security:authentication property="principal.username"/>
		</td>
		<td>
			<form action='${logoutUrl}' method='POST'>
	    		<input type="submit" value='Logout'>
	    	</form>    				
    	</td>
    </tr>
</security:authorize>

<a class="locale_selector" href="#" name="ru_RU">RU</a>
<a class="locale_selector" href="#" name="en_US">EN</a>
<hr>

package com.epam.newsmanagement.command.impl;

import javax.servlet.http.HttpServletRequest;

import com.epam.newsmanagement.command.Command;
import com.epam.newsmanagement.util.SearchCriteria;

public class SetSearchCriteriaCommand implements Command {

	@Override
	public String execute(HttpServletRequest request) {
		String authorStr = request.getParameter("author");
		String[] tagsStr = request.getParameterValues("selectedTags[]");
		SearchCriteria searchCriteria = new SearchCriteria();
		if(authorStr != null && !authorStr.isEmpty()){
			searchCriteria.setAuthorId(Long.parseLong(authorStr));
		}
		if(tagsStr != null && tagsStr.length > 0){
			for(String tagStr: tagsStr){
				searchCriteria.addTagId(Long.parseLong(tagStr));
			}
		}
		request.getSession().setAttribute("searchCriteria",searchCriteria);
		return new SearchCommand().execute(request);
	}

}

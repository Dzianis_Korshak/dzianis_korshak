package com.epam.newsmanagement.command;

import javax.servlet.http.HttpServletRequest;

import com.epam.newsmanagement.exception.ServiceException;

public interface Command {
	
	public String execute(HttpServletRequest request) throws ServiceException;
	
}

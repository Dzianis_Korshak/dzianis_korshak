package com.epam.newsmanagement.command.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.epam.newsmanagement.command.Command;
import com.epam.newsmanagement.command.util.ConfigurationManager;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.NewsManager;
import com.epam.newsmanagement.util.ApplicationContextUtil;
import com.epam.newsmanagement.util.SearchCriteria;

public class DisplayNewsCommand implements Command {

	@Override
	public String execute(HttpServletRequest request) throws ServiceException {
		NewsManager newsManager = (NewsManager) ApplicationContextUtil.getApplicationContext().getBean("newsManager");
		long newsId = Integer.parseInt(request.getParameter("newsId"));
		List<News> newsTripplet = newsManager.getNewsTripplet(getSearchCriteria(request.getSession()), newsId);
		request.setAttribute("prev", newsTripplet.get(0));
		request.setAttribute("article", newsTripplet.get(1));
		request.setAttribute("next", newsTripplet.get(2));
		return ConfigurationManager.getProperty("page.news");
	}
	
	private SearchCriteria getSearchCriteria(HttpSession session){
		SearchCriteria searchCriteria = (SearchCriteria)session.getAttribute("searchCriteria");
		if(searchCriteria == null){
			searchCriteria = new SearchCriteria();
			session.setAttribute("searchCriteria", searchCriteria);
		}
		return searchCriteria;
	}
}

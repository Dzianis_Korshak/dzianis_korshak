package com.epam.newsmanagement.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epam.newsmanagement.command.Command;
import com.epam.newsmanagement.command.factory.CommandFactory;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.util.ApplicationContextUtil;

public class ClientServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public void init(){
    	ApplicationContextUtil.setAppliactionContext("classpath*:spring.xml");
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Command command = CommandFactory.defineCommand(request);
		String view = null;
		try {
			view = command.execute(request);
		} catch (ServiceException e) {
			
		}
		if(view != null){
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(view);
			dispatcher.forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}

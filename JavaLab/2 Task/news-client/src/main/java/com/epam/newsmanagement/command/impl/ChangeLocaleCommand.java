package com.epam.newsmanagement.command.impl;

import javax.servlet.http.HttpServletRequest;

import com.epam.newsmanagement.command.Command;

public class ChangeLocaleCommand implements Command {

	@Override
	public String execute(HttpServletRequest request) {
		String localeSelected = request.getParameter("localeSelected");
		request.getSession().setAttribute("locale", localeSelected);
		return null;
	}

}

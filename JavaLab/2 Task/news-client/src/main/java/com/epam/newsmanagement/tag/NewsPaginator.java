package com.epam.newsmanagement.tag;

import java.io.IOException;
import java.util.List;

import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyTagSupport;

import com.epam.newsmanagement.entity.News;

@SuppressWarnings("serial")
public class NewsPaginator extends BodyTagSupport {
	private List<News> newsList;
	private long pageNumber = 1;
	private long totalNewsNumber = 3;
	private long newsPerPage = 3;
	private String paginationUrlPrefix;
	private String viewNewsUrlPrefix;
	private int current = 0;
	
	public void setNewsList(List<News> newsList){
		this.newsList = newsList;
	}
	
	public void setPageNumber(long pageNumber){
		this.pageNumber = pageNumber;
	}
	
	public void setTotalNewsNumber(long totalNewsNumber){
		this.totalNewsNumber = totalNewsNumber;
	}
	
	public void setNewsPerPage(long newsPerPage){
		this.newsPerPage = newsPerPage;
	}
	
	public void setPaginationUrlPrefix(String paginationUrlPrefix){
		this.paginationUrlPrefix = paginationUrlPrefix;
	}
	
	public void setViewNewsUrlPrefix(String viewNewsUrlPrefix){
		this.viewNewsUrlPrefix = viewNewsUrlPrefix;
	}
	
	public int doStartTag() throws JspTagException {
		if (newsList == null || newsList.isEmpty()){
			return SKIP_BODY;
		}
		totalNewsNumber = totalNewsNumber == 0 ? newsList.size() : totalNewsNumber;
		return EVAL_BODY_BUFFERED;
	}
	
	public int doAfterBody() throws JspTagException{
		if(current >= newsList.size()){
			return SKIP_BODY;
		}
		try {
			writeSingleNews(newsList.get(current), bodyContent.getEnclosingWriter());
			attachBody(newsList.get(current).getNews().getId(), bodyContent.getEnclosingWriter());
			current++;
			return EVAL_BODY_AGAIN;
		} catch (IOException e) {
			throw new JspTagException(e.getMessage());
		}
	}
	
	private void writeSingleNews(News news, JspWriter out) throws IOException{
		out.write("<h3><a class='news-title' href='" + viewNewsUrlPrefix + news.getNews().getId() + "'>"+news.getNews().getTitle()+"</a></h3>");
		out.write("<span class='news-author'>("+news.getAuthor().getName()+")</span>");
		out.write("<span class='news-date'>"+news.getNews().getModificationDate()+"</span>");
		out.write("<div class='news-shor-text'>"+news.getNews().getShortText()+"</div");
		StringBuilder tags = new StringBuilder();
		for(int i = 0; i < news.getTagList().size(); i++){
			tags.append(news.getTagList().get(i).getTagName());
			if(i + 1 != news.getTagList().size()){
				tags.append(", ");
			}
		}
		out.write("<span class='news-tags'>" + tags + "</span>");
		out.write("<span class='news-comment'>Comments(" + news.getCommentList().size() + ")</span>");
	}
	
	private void attachBody(long news, JspWriter out) throws JspTagException{
		if(bodyContent != null){
			String htmlContent = bodyContent.getString();
			bodyContent.clearBody();
			String resContent = htmlContent.replaceFirst("\\{newsId\\}", ""+news);
			try {
				out.write(resContent);
			} catch (IOException e) {
				throw new JspTagException(e.getMessage());
			}
		}
	}
	
	public int doEndTag() throws JspTagException{
		if (newsList != null && !newsList.isEmpty()){
			doPagination(bodyContent.getEnclosingWriter());
		}
		current = 0;
		return EVAL_PAGE;
	}
	
	private void doPagination(JspWriter out) throws JspTagException{
		long totalPageNumber = (long)Math.ceil((double)totalNewsNumber / newsPerPage);
		try {
			for(long i = 1; i <= totalPageNumber; i++){
				if(i == pageNumber){
					out.append("<a class='pagination-button selected-page' href='" + paginationUrlPrefix + i +"'>"+i+"</a>");
				} else {
					out.append("<a class='pagination-button' href='" + paginationUrlPrefix + i +"'>"+i+"</a>");
				}
			}
		} catch (IOException e) {
			throw new JspTagException(e.getMessage());
		}
	}
	
}

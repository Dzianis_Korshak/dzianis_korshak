package com.epam.newsmanagement.command.impl;

import javax.servlet.http.HttpServletRequest;

import com.epam.newsmanagement.command.Command;

public class EmptyCommand implements Command {

	@Override
	public String execute(HttpServletRequest request) {
		return "";
	}

}

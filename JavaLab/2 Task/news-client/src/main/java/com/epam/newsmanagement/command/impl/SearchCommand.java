package com.epam.newsmanagement.command.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.epam.newsmanagement.command.Command;
import com.epam.newsmanagement.command.util.ConfigurationManager;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.dto.AuthorDTO;
import com.epam.newsmanagement.entity.dto.TagDTO;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.NewsManager;
import com.epam.newsmanagement.util.ApplicationContextUtil;
import com.epam.newsmanagement.util.SearchCriteria;

public class SearchCommand implements Command {

	@Override
	public String execute(HttpServletRequest request) {
		NewsManager newsManager = (NewsManager) ApplicationContextUtil.getApplicationContext().getBean("newsManager");
		int page = getPageNumber(request);
		int newsPerPage = getNewsPerPageNumber(request);
		try {
			SearchCriteria searchCriteria = getSearchCriteria(request.getSession());
			List<News> listNews = newsManager.search(searchCriteria,(page-1)*newsPerPage+1,newsPerPage);
			long totalNewsNumber = newsManager.getNewsNumber(searchCriteria);
			List<TagDTO> tagList = newsManager.getTags();
			List<AuthorDTO> authorList = newsManager.getAuthors();
			request.setAttribute("tagList", tagList);
			request.setAttribute("authorList", authorList);
			request.setAttribute("newsList", listNews);
			request.setAttribute("totalNewsNumber", totalNewsNumber);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return ConfigurationManager.getProperty("page.home");
	}
	
	private SearchCriteria getSearchCriteria(HttpSession session){
		SearchCriteria searchCriteria = (SearchCriteria)session.getAttribute("searchCriteria");
		if(searchCriteria == null){
			searchCriteria = new SearchCriteria();
			session.setAttribute("searchCriteria", searchCriteria);
		}
		return searchCriteria;
	}
	
	private int getPageNumber(HttpServletRequest request){
		String paramValue = request.getParameter("page");
		if (paramValue == null){
			return 1;
		}
		return Integer.parseInt(paramValue);
	}
	
	private int getNewsPerPageNumber(HttpServletRequest request){
		Object attrValue = request.getSession().getAttribute("newsPerPage");
		if(attrValue == null || !(attrValue instanceof Integer)){
			Integer defaultValue = Integer.parseInt(ConfigurationManager.getProperty("config.news-per-page"));
			request.getSession().setAttribute("newsPerPage", defaultValue);
			return defaultValue;
		}
		return (Integer)attrValue;
	}

}

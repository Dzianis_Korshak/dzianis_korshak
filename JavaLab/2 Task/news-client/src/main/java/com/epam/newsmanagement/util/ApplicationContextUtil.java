package com.epam.newsmanagement.util;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class ApplicationContextUtil {
	
	private static ApplicationContext applicationContext;
	
	public static void setAppliactionContext(String configLocation){
		applicationContext = new ClassPathXmlApplicationContext(configLocation);
	}
	
	public static ApplicationContext getApplicationContext(){
		return applicationContext;
	}
	
}

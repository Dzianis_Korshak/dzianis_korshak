package com.epam.newsmanagement.command.util;

import com.epam.newsmanagement.command.Command;
import com.epam.newsmanagement.command.impl.ChangeLocaleCommand;
import com.epam.newsmanagement.command.impl.CommentCommand;
import com.epam.newsmanagement.command.impl.DisplayNewsCommand;
import com.epam.newsmanagement.command.impl.SearchCommand;
import com.epam.newsmanagement.command.impl.SetSearchCriteriaCommand;

public enum CommandEnum {
	
	SEARCH {
		{
			this.command = new SearchCommand();
		}
	},
	COMMENT {
		{
			this.command = new CommentCommand();
		}
	},
	DISPLAY {
		{
			this.command = new DisplayNewsCommand();
		}		
	},
	SET_CRITERIA {
		{
			this.command = new SetSearchCriteriaCommand();
		}
	},
	CHANGE_LOCALE {
		{
			this.command = new ChangeLocaleCommand();
		}
	};
	Command command;
	public Command getCommand(){
		return command;
	}
}

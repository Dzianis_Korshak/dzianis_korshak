package com.epam.newsmanagement.command.factory;

import javax.servlet.http.HttpServletRequest;

import com.epam.newsmanagement.command.Command;
import com.epam.newsmanagement.command.impl.EmptyCommand;
import com.epam.newsmanagement.command.util.CommandEnum;

public class CommandFactory {

	public static Command defineCommand(HttpServletRequest request){
		String action = request.getParameter("command");
		if(action == null || action.isEmpty()){
			return new EmptyCommand();
		}
		try {
			CommandEnum commandEnum = CommandEnum.valueOf(action.toUpperCase());
			return commandEnum.getCommand();
		} catch(IllegalArgumentException ex){
			request.setAttribute("wrongAction", action);
		}
		return new EmptyCommand();
	}
}

package com.epam.newsmanagement.command.impl;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import com.epam.newsmanagement.command.Command;
import com.epam.newsmanagement.command.util.ConfigurationManager;
import com.epam.newsmanagement.command.util.MessageManager;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.dto.CommentDTO;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.NewsManager;
import com.epam.newsmanagement.util.ApplicationContextUtil;

public class CommentCommand implements Command {

	@Override
	public String execute(HttpServletRequest request) {
		NewsManager newsManager = (NewsManager) ApplicationContextUtil.getApplicationContext().getBean("newsManager");
		long newsId = Integer.parseInt(request.getParameter("newsId"));
		String commentText = request.getParameter("commentText");
		if (!isValid(commentText)){
			System.out.println("Op!");
			request.setAttribute("errMsg", MessageManager.getProperty("message.error.comment"));
			return ConfigurationManager.getProperty("page.comments");
		}
		CommentDTO comment = new CommentDTO();
		comment.setNewsId(newsId);
		comment.setCommentText(commentText);
		comment.setCreationDate(new Date());
		try {
			newsManager.addComment(comment);
			News news = newsManager.getNews(newsId);
			request.setAttribute("article", news);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return ConfigurationManager.getProperty("page.comments");
	}
	
	private boolean isValid(String commentText){
		return commentText != null && commentText.length() > 0 && commentText.length() <= 100;
	}

}

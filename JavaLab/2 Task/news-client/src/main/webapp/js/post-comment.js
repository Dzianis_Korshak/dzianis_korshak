/**
 * 
 */
$(document).ready(function(){
	$.urlParam = function(name){
	    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
	    if (results==null){
	       return null;
	    }
	    else{
	       return results[1] || 0;
	    }
	};
	
	$('#post-comment').click(function(event) {
		var command = "comment";
		var newsId = $.urlParam('newsId');
		var commentText = $('textarea[name="commentText"]').val();
		$.post('ClientServlet',{command:command,newsId:newsId,commentText:commentText},function(data){
	        $('.comment-list').html(data);
		});
	});
});
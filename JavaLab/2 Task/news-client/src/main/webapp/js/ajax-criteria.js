$(document).ready(function(){
	var optionText = $('.multiselect option[selected]').text();
	
	var setSelectedTagNumber = function(){
		var selectedTags = [];
        $('input[name="selected-tags[]"]:checked').each(function(i){
        	selectedTags[i] = $(this).val();
        });
        $('.multiselect option[selected]').text(optionText + '(' + selectedTags.length + ')');
	};
	
	setSelectedTagNumber();
	
	$('#search').click(function(event) {
		var command = "set_criteria";
		var author = $('select[name="selected-author"]').val();
		var selectedTags = [];
        $('input[name="selected-tags[]"]:checked').each(function(i){
        	selectedTags[i] = $(this).val();
        });
		$.post('ClientServlet',{command:command,author:author,'selectedTags[]':selectedTags},function(data){
	        $('body').html(data);
		});
	});
	
	$('#reset').click(function(event) {
		var command = "set_criteria";
		$.post('ClientServlet',{command:command},function(data){
	        $('body').html(data);
		});
	});
	
	$('input[name="selected-tags[]"]').click(function(event){
		setSelectedTagNumber();
	});
});
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<c:forEach items="${article.commentList}" var="comment">
		<div class="date">${comment.creationDate }</div>
		<div class="comment-text">${comment.commentText }</div>
	</c:forEach>
	${errMsg }
	<form class="comment-form">
		<input type="hidden" name="command" value="comment">
		<textarea rows="4" cols="60" name="commentText"></textarea>
		<input id="post-comment" type="button" value="Post comment"></input>
	</form>
</body>
</html>
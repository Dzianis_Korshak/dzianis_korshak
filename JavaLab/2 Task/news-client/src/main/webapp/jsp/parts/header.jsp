<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<script src="${pageContext.request.contextPath}/js/jquery-2.1.1.min.js"></script>
	<script src="${pageContext.request.contextPath}/js/changeLocale.js"></script>
	<link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet"/>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Insert title here</title>
	<fmt:setLocale value="${locale}" scope="session" />
	<fmt:setBundle basename="pagecontent" var="rb"/>
</head>
<body>
	<h1><fmt:message key="label.header" bundle="${rb}" /></h1>
	<a class="locale_selector" href="#" name="ru_RU">RU</a>
	<a class="locale_selector" href="#" name="en_US">EN</a>
</body>
</html>
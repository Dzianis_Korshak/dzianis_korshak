<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<script src="${pageContext.request.contextPath}/js/jquery-2.1.1.min.js"></script>
	<script src="${pageContext.request.contextPath}/js/post-comment.js"></script>
	<script src="${pageContext.request.contextPath}/js/back-to-home.js"></script>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<fmt:setLocale value="${locale }" scope="session" />
	<fmt:setBundle basename="resources.pagecontent" var="rb"/>
<title>Insert title here</title>
</head>
<body>
	<c:import url="/jsp/parts/header.jsp"></c:import>
	<a href="ClientServlet?command=search" id="back-ref">Back</a>
	<div class="content">
		<div class="news-title">
			${article.news.title}
		</div>
		<span class="author">(${article.author.name})</span>
		<div class="date">${article.news.modificationDate }</div>
		<div class="news-text">${article.news.fullText }</div>
		<div class="comment-list">
			<c:import url="/jsp/parts/comment-list.jsp"></c:import>
		</div>
	</div>
	<c:if test="${prev != null}">
		<a href="ClientServlet?command=display&newsId=${prev.news.id}">PREV</a>
	</c:if>
	<c:if test="${next != null}">
		<a href="ClientServlet?command=display&newsId=${next.news.id}">NEXT</a>
	</c:if>
	${errMsg}
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="ctg" uri="news-paginator" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<script src="${pageContext.request.contextPath}/js/jquery-2.1.1.min.js"></script>
	<script src="${pageContext.request.contextPath}/js/multi-select.js"></script>
	<script src="${pageContext.request.contextPath}/js/ajax-criteria.js"></script>
	<link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet"/>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<fmt:setLocale value="${locale}" scope="session" />
	<fmt:setBundle basename="pagecontent" var="rb"/>
<title>Home</title>
</head>
<body>
	<c:import url="/jsp/parts/header.jsp"></c:import>
	<form>
		<div class="select-wrapper">
			<select name="selected-author">
	    		<option disabled selected="selected">Select an option</option>
	   			<c:forEach items="${authorList}" var="author">
					<option value="${author.id}" ${author.id == searchCriteria.authorId ? 'selected="selected"' : ''}>${author.name }</option>
				</c:forEach>
	   		</select>
		</div>
	    <div class="select-wrapper">
	        <div class="select-box multiselect">
	            <select>
	                <option selected disabled>Select an option</option>
	            </select>
	            <div class="overSelect"></div>
	        </div>
	        <div class="checkboxes">
	        	<label><input disabled type="checkbox"/>Select an option</label>
	        	<c:forEach items="${tagList}" var="tag">
					<label><input name="selected-tags[]" type="checkbox" value="${tag.id}"
						${searchCriteria.contains(tag.id) ? 'checked="checked"' : ''}/>${tag.tagName }</label>
				</c:forEach>
	        </div>
	    </div>
	    <input type="button" id="search" value="Filter"/>
	    <input type="button" id="reset" value="Reset"/>
	</form>
	
	<ctg:news newsList="${newsList}" pageNumber="${page}" totalNewsNumber="${totalNewsNumber}" 
		newsPerPage="${newsPerPage }" paginationUrlPrefix="ClientServlet?command=search&page="
		viewNewsUrlPrefix="ClientServlet?command=display&newsId=">
		<a href="ClientServlet?command=display&newsId={newsId}">View</a>
	</ctg:news>
	
</body>
</html>